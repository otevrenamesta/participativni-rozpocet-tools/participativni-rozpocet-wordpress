<?php
wp_enqueue_script('', get_template_directory_uri() . '/assets/js/front-page.js?'.rand(), array('jquery', 'damenavas-paro2'), '', true);
global $damenavas_defaults;
/**
 * The front page template file.
 *
 * @package vega
 */
?>
<?php get_header(); ?>
<?php
wp_body_open();
?>

<?php if (get_theme_mod('show_hero_section')): ?>
    <div class="container-fluid main-image-container">
        <div class="row main-image-row">
            <div class="col-sm-2 wow bounceIn" data-wow-delay="0.3s" data-wow-duration="0.8s"></div>
            <div class="col-sm-5 title">
                <h1><?php echo get_theme_mod('frontpage_text_1', $damenavas_defaults['frontpage_text_1']); ?></h1>
                <h2><?php echo get_theme_mod('frontpage_text_2', $damenavas_defaults['frontpage_text_2']); ?></h2>
                <h2><?php echo get_theme_mod('frontpage_text_3', $damenavas_defaults['frontpage_text_3']); ?></h2>
                <div class="button clearfix">
                    <a href="<?php echo get_permalink(get_theme_mod('frontpage_link', $damenavas_defaults['frontpage_link'])); ?>"
                       class="p-button"
                       type="button"><?php echo get_theme_mod('frontpage_text_4', $damenavas_defaults['frontpage_text_4']); ?></a>
                </div>
            </div>
            <?php if (get_theme_mod('show_image_slider', $damenavas_defaults['show_image_slider'])): ?>
                <div class="col-sm-4 image" style="z-index: 10;">
                    <div id="carousel-front-page" class="carousel slide hidden-xs" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <?php
                        $images = explode(',', get_theme_mod('image_slider_media'/*no default expected*/));
                        $imageActive = true;
                        ?>
                        <div class="carousel-inner" role="listbox">
                            <?php foreach ($images as $image) { if (empty($image)) {continue;}?>
                                <div class="item <?php if ($imageActive) {
                                    echo ' active ';
                                }
                                $imageActive = false; ?>">
                                    <img style="background-image: url('<?php echo $image; ?>');">
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="row main-image-row-bottom hidden-xs">
            <div class="scroll-info">Pokračujte scrollováním</div>
        </div>
    </div>
<?php endif; ?>
</div>
<div class="section blog-feed bg-white">

    <?php if (!get_theme_mod('show_hero_section', $damenavas_defaults['show_hero_section'])): ?>
        <div style="padding-top: 3rem;"></div>
    <?php endif; ?>

    <?php if (get_theme_mod('show_3points', $damenavas_defaults['show_3points'])): ?>
        <div class="container-fluid first-section-fluid">

            <div class="row first-section equal">
                <div class="col-sm-2"></div>

                <div class="col-sm-3 col-xs-12 one-block">
                    <div class="row equal">
                        <div class="col-xs-12 one-block-title svg-1">
                            <h2 class="number">1.</h2>
                            <h2 class="title"><?= get_theme_mod('3points_1_title', $damenavas_defaults['3points_1_title']) ?></h2>
                        </div>
                        <div class="col-xs-12 one-block-description">
                            <div class="description">
                                <?= get_theme_mod('3points_1_text', $damenavas_defaults['3points_1_text']) ?>

                                <a href="<?= get_permalink(get_theme_mod('3points_1_button_link', $damenavas_defaults['3points_1_button_link'])) ?>"
                                   class="btn btn-default"
                                   style="padding: 12px 0; font-family: 'Raleway', sans-serif; margin-top: 20px; width: 100%; background: #da2128; color: white; font-weight:bold"
                                   type="button"><?= get_theme_mod('3points_1_button_text', $damenavas_defaults['3points_1_button_text']) ?></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-xs-12 one-block">
                    <div class="row equal">
                        <div class="col-xs-12 one-block-title svg-2">
                            <h2 class="number">2.</h2>
                            <h2 class="title"><?= get_theme_mod('3points_2_title', $damenavas_defaults['3points_2_title']) ?></h2>
                        </div>
                        <div class="col-xs-12 one-block-description">
                            <div class="description">
                                <?= get_theme_mod('3points_2_text', $damenavas_defaults['3points_2_text']) ?>

                                <a href="<?= get_permalink(get_theme_mod('3points_2_button_link', $damenavas_defaults['3points_2_button_link'])) ?>"
                                   class="btn btn-default"
                                   style="padding: 12px 0; font-family: 'Raleway', sans-serif; margin-top: 20px; width: 100%; background: #da2128; color: white; font-weight:bold"
                                   type="button"><?= get_theme_mod('3points_2_button_text', $damenavas_defaults['3points_2_button_text']) ?></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-xs-12 one-block">
                    <div class="row equal">
                        <div class="col-xs-12 one-block-title svg-3">
                            <h2 class="number">3.</h2>
                            <h2 class="title">
                                <?= get_theme_mod('3points_3_title', $damenavas_defaults['3points_3_title']) ?>
                            </h2>
                        </div>
                        <div class="col-xs-12 one-block-description">
                            <div class="description">
                                <?= get_theme_mod('3points_3_text', $damenavas_defaults['3points_3_text']) ?>

                                <a href="<?= get_permalink(get_theme_mod('3points_3_button_link', $damenavas_defaults['3points_3_button_link'])) ?>"
                                   class="btn btn-default"
                                   style="padding: 12px 0; font-family: 'Raleway', sans-serif; margin-top: 20px; width: 100%; background: #da2128; color: white; font-weight:bold"
                                   type="button"><?= get_theme_mod('3points_3_button_text', $damenavas_defaults['3points_3_button_text']) ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (get_theme_mod('show_news_section', $damenavas_defaults['show_news_section'])): ?>
        <div class="container-fluid sixth-section-fluid">
            <?php
            // Get the ID of a given category
            $category_id = get_theme_mod('frontpage_news_category');

            if (!empty($category_id)):
                // Get the URL of this category
                $category_link = get_category_link($category_id);
                ?>
                <div class="row sixth-section">
                    <div class="col-sm-2 left-title wow bounceInLeft" data-wow-delay="0.3s" data-wow-offset="100">
                        <a href="<?= $category_link ?>" style="color: black"><h3><?= __('Aktuality') ?></h3></a>
                        <div class="arrow-icon">
                            <img src="<?= get_theme_file_uri('assets/img/arrow_red.png') ?>">
                        </div>
                    </div>

                    <?php
                    $args = array('posts_per_page' => 3, 'offset' => 0, 'category' => $category_id);

                    $myposts = get_posts($args);
                    foreach ($myposts as $post) : setup_postdata($post); ?>
                        <div class="col-sm-3 col-xs-12 sixth-block-title">
                            <?php the_post_thumbnail(); ?>
                            <a href="<?php the_permalink(); ?>" style="color: black"><h4><?php the_title(); ?></h4></a>
                            <h5><?php the_time('j.n.Y'); ?></h5>
                            <?php the_excerpt(); ?>
                        </div>
                    <?php
                    endforeach;
                    ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (get_theme_mod('show_harmonogram', $damenavas_defaults['show_harmonogram'])): ?>
        <div class="container-fluid">
            <div class="row fifth-section">
                <div class="col-sm-2 col-xs-12 fifth-left-block">
                </div>
                <div class="col-sm-3 fifth-center-block">
                <span class="fifth-left-description" id="harmonogram-target">

                </span>
                </div>
                <div class="col-sm-6 col-xs-12 fifth-right-block">
                    <h2 class="col-xs-12"><?= get_theme_mod('frontpage_harmonogram_title', $damenavas_defaults['frontpage_harmonogram_title']) ?></h2>
                    <div class="col-xs-12">
                        <?= get_theme_mod('frontpage_harmonogram_subtitle', $damenavas_defaults['frontpage_harmonogram_subtitle']) ?>
                    </div>
                    <div class="col-xs-12">
                        <?= get_theme_mod('frontpage_harmonogram_text', $damenavas_defaults['frontpage_harmonogram_text']) ?>
                    </div>
                    <div class="col-xs-12 fifth-block-button">
                        <a href="<?= get_permalink(get_theme_mod('frontpage_harmonogram_button_link', $damenavas_defaults['frontpage_harmonogram_button_link'])) ?>"
                           class="p-button"
                           type="button">
                            <?= get_theme_mod('frontpage_harmonogram_button_text', $damenavas_defaults['frontpage_harmonogram_button_text']) ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (get_theme_mod('show_happening_now', $damenavas_defaults['show_happening_now'])): ?>
        <div class="container-fluid fourth-section-fluid">
            <div class="row fourth-section">
                <div class="col-sm-2"></div>
                <div class="col-sm-3 col-xs-12 fourth-block-title">
                    <div class="col-xs-12"><?= get_theme_mod('frontpage_happening_now_title', $damenavas_defaults['frontpage_happening_now_title']) ?></div>
                </div>
                <div class="col-sm-3 col-xs-12 fourth-section-description">
                    <h4><?= get_theme_mod('frontpage_happening_now_text', $damenavas_defaults['frontpage_happening_now_text']) ?></h4>
                </div>
                <div class="col-sm-4 col-xs-12 fourth-block-button">
                    <a href="<?= get_permalink(get_theme_mod('frontpage_happening_now_button_link', $damenavas_defaults['frontpage_happening_now_button_link'])) ?>"
                       class="p-button" type="button">
                        <?= get_theme_mod('frontpage_happening_now_button_title', $damenavas_defaults['frontpage_happening_now_button_title']) ?>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php get_footer(); ?>
