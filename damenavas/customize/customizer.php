<?php
/**
 * Customizer functionality
 *
 * @package vega
 */
?>
<?php

/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
function vega_wp_customize_register(WP_Customize_Manager $wp_customize)
{
    global $damenavas_defaults;

    $wp_customize->add_section('damenavas_backend', [
        'title' => 'Integrace s PaRo2',
        'priority' => 1
    ]);
    $wp_customize->add_setting('api_url', [
        'sanitize_callback' => 'vega_wp_sanitize_url'
    ]);
    $wp_customize->add_control('api_url', [
        'type' => 'url',
        'label' => __('URL adresa API PaRo2'),
        'section' => 'damenavas_backend',
    ]);
    $wp_customize->add_setting('api_organization_id', [
        'sanitize_callback' => 'vega_wp_sanitize_number'
    ]);
    $wp_customize->add_control('api_organization_id', [
        'type' => 'number',
        'label' => __('ID Organizace'),
        'section' => 'damenavas_backend',
    ]);
    $wp_customize->add_setting('api_current_year', [
        'sanitize_callback' => 'vega_wp_sanitize_number'
    ]);
    $wp_customize->add_control('api_current_year', [
        'type' => 'number',
        'label' => __('Aktuální ročník (rok, např. 2021)'),
        'section' => 'damenavas_backend',
    ]);

    /**
     * Obecné nastavení hlavní stránky
     * */

    $wp_customize->add_section('damenavas_frontpage', [
        'title' => __('Hlavní stránka'),
        'priority' => 1
    ]);

    $wp_customize->add_setting('show_hero_section', [
        'default' => $damenavas_defaults['show_hero_section'],

    ]);
    $wp_customize->add_control('show_hero_section', [
        'type' => 'checkbox',
        'label' => __('Zobrazit velkou úvodní hlavičku'),
        'section' => 'damenavas_frontpage'
    ]);

    $wp_customize->add_setting('show_image_slider', [
        'default' => $damenavas_defaults['show_image_slider'],

    ]);
    $wp_customize->add_control('show_image_slider', [
        'type' => 'checkbox',
        'label' => __('Zobrazit galerii v hlavičce'),
        'section' => 'damenavas_frontpage'
    ]);

    $wp_customize->add_setting('show_3points', [
        'default' => $damenavas_defaults['show_3points'],

    ]);
    $wp_customize->add_control('show_3points', [
        'type' => 'checkbox',
        'label' => __('Zobrazit sekci 3 hlavní body'),
        'section' => 'damenavas_frontpage'
    ]);

    $wp_customize->add_setting('show_news_section', [
        'default' => $damenavas_defaults['show_news_section'],
    ]);
    $wp_customize->add_control('show_news_section', [
        'type' => 'checkbox',
        'label' => __('Zobrazit sekci Aktuality'),
        'section' => 'damenavas_frontpage'
    ]);

    $wp_customize->add_setting('show_harmonogram', [
        'default' => $damenavas_defaults['show_harmonogram'],

    ]);
    $wp_customize->add_control('show_harmonogram', [
        'type' => 'checkbox',
        'label' => __('Zobrazit sekci Harmonogram'),
        'section' => 'damenavas_frontpage'
    ]);

    $wp_customize->add_setting('show_happening_now', [
        'default' => $damenavas_defaults['show_happening_now'],

    ]);
    $wp_customize->add_control('show_happening_now', [
        'type' => 'checkbox',
        'label' => __('Zobrazit sekci "Co se děje teď"'),
        'section' => 'damenavas_frontpage'
    ]);

    /**
     * Nastavení hlavičky na hlavní stránce
     */

    $wp_customize->add_section('damenavas_frontpage_header', [
        'title' => __('&nbsp;&nbsp;&nbsp;&nbsp;Záhlaví', 'vega'),
        'priority' => 2,
    ]);

    $wp_customize->add_setting('frontpage_text_1', [
        'default' => $damenavas_defaults['frontpage_text_1'],
        'sanitize_callback' => 'vega_wp_sanitize_text',

    ]);
    $wp_customize->add_control('frontpage_text_1', [
        'label' => __('Hlavní nadpis'),
        'type' => 'text',
        'section' => 'damenavas_frontpage_header'
    ]);

    $wp_customize->add_setting('frontpage_text_2', [
        'default' => $damenavas_defaults['frontpage_text_2'],
        'sanitize_callback' => 'vega_wp_sanitize_text',

    ]);
    $wp_customize->add_control('frontpage_text_2', [
        'label' => __('Druhý řádek'),
        'type' => 'text',
        'section' => 'damenavas_frontpage_header'
    ]);

    $wp_customize->add_setting('frontpage_text_3', [
        'default' => $damenavas_defaults['frontpage_text_3'],
        'sanitize_callback' => 'vega_wp_sanitize_text',

    ]);
    $wp_customize->add_control('frontpage_text_3', [
        'label' => __('Poslední řádek'),
        'type' => 'text',
        'section' => 'damenavas_frontpage_header'
    ]);

    $wp_customize->add_setting('frontpage_text_4', [
        'default' => $damenavas_defaults['frontpage_text_4'],
        'sanitize_callback' => 'vega_wp_sanitize_text',

    ]);
    $wp_customize->add_control('frontpage_text_4', [
        'label' => __('Text akčního tlačítka'),
        'type' => 'text',
        'section' => 'damenavas_frontpage_header'
    ]);

    $wp_customize->add_setting('frontpage_link', [
        'default' => $damenavas_defaults['frontpage_link'],
        'sanitize_callback' => 'vega_wp_sanitize_page',

    ]);
    $wp_customize->add_control('frontpage_link', [
        'label' => __('Odkaz akčního tlačítka'),
        'type' => 'dropdown-pages',
        'section' => 'damenavas_frontpage_header'
    ]);

    $wp_customize->add_setting('image_slider_media', [
        'default' => ''
    ]);
    $wp_customize->add_control(new Multi_Image_Custom_Control($wp_customize, 'image_slider_media', [
        'label' => __('Vyberte obrázky k zobrazení v galerii'),
        'section' => 'damenavas_frontpage_header'
    ]));

    $wp_customize->add_section('vega_frontpage_3points', [
        'title' => __('&nbsp;&nbsp;&nbsp;&nbsp;3 hlavní body', 'vega'),
        'priority' => 2,
    ]);

    $wp_customize->add_setting('3points_1_title', [
        'default' => $damenavas_defaults['3points_1_title'],
        'sanitize_callback' => 'vega_wp_sanitize_text',

    ]);
    $wp_customize->add_control('3points_1_title', [
        'label' => __('Nadpis 1. bodu'),
        'type' => 'text',
        'section' => 'vega_frontpage_3points'
    ]);
    $wp_customize->add_setting('3points_1_text', [
        'default' => $damenavas_defaults['3points_1_text'],
        'sanitize_callback' => 'vega_wp_sanitize_html'
    ]);
    $wp_customize->add_control(new RichText_Editor_Custom_Control($wp_customize, '3points_1_text', [
        'label' => __('Popis 1. bodu'),
        'type' => 'textarea',
        'section' => 'vega_frontpage_3points'
    ]));
    $wp_customize->add_setting('3points_1_button_text', [
        'default' => $damenavas_defaults['3points_1_button_text'],
        'sanitize_callback' => 'vega_wp_sanitize_text',

    ]);
    $wp_customize->add_control('3points_1_button_text', [
        'label' => __('Text akčního tlačítka'),
        'type' => 'text',
        'section' => 'vega_frontpage_3points'
    ]);
    $wp_customize->add_setting('3points_1_button_link', [
        'default' => $damenavas_defaults['3points_1_button_link'],
        'sanitize_callback' => 'vega_wp_sanitize_page',
    ]);
    $wp_customize->add_control('3points_1_button_link', [
        'label' => __('Odkaz akčního tlačítka'),
        'type' => 'dropdown-pages',
        'section' => 'vega_frontpage_3points'
    ]);

    $wp_customize->add_setting('3points_2_title', [
        'default' => $damenavas_defaults['3points_2_title'],
        'sanitize_callback' => 'vega_wp_sanitize_text',
    ]);
    $wp_customize->add_control('3points_2_title', [
        'label' => __('Nadpis 2. bodu'),
        'type' => 'text',
        'section' => 'vega_frontpage_3points'
    ]);
    $wp_customize->add_setting('3points_2_text', [
        'default' => $damenavas_defaults['3points_2_text'],
        'sanitize_callback' => 'vega_wp_sanitize_html',

    ]);
    $wp_customize->add_control(new RichText_Editor_Custom_Control($wp_customize, '3points_2_text', [
        'label' => __('Popis 2. bodu'),
        'type' => 'textarea',
        'section' => 'vega_frontpage_3points'
    ]));
    $wp_customize->add_setting('3points_2_button_text', [
        'default' => $damenavas_defaults['3points_2_button_text'],
        'sanitize_callback' => 'vega_wp_sanitize_text',

    ]);
    $wp_customize->add_control('3points_2_button_text', [
        'label' => __('Text akčního tlačítka'),
        'type' => 'text',
        'section' => 'vega_frontpage_3points'
    ]);
    $wp_customize->add_setting('3points_2_button_link', [
        'default' => $damenavas_defaults['3points_2_button_link'],
        'sanitize_callback' => 'vega_wp_sanitize_page',
    ]);
    $wp_customize->add_control('3points_2_button_link', [
        'label' => __('Odkaz akčního tlačítka'),
        'type' => 'dropdown-pages',
        'section' => 'vega_frontpage_3points'
    ]);

    $wp_customize->add_setting('3points_3_title', [
        'default' => $damenavas_defaults['3points_3_title'],
        'sanitize_callback' => 'vega_wp_sanitize_text',

    ]);
    $wp_customize->add_control('3points_3_title', [
        'label' => __('Nadpis 3. bodu'),
        'type' => 'text',
        'section' => 'vega_frontpage_3points'
    ]);
    $wp_customize->add_setting('3points_3_text', [
        'default' => $damenavas_defaults['3points_3_text'],
        'sanitize_callback' => 'vega_wp_sanitize_html',

    ]);
    $wp_customize->add_control(new RichText_Editor_Custom_Control($wp_customize, '3points_3_text', [
        'label' => __('Popis 3. bodu'),
        'type' => 'textarea',
        'section' => 'vega_frontpage_3points'
    ]));
    $wp_customize->add_setting('3points_3_button_text', [
        'default' => $damenavas_defaults['3points_3_button_text'],
        'sanitize_callback' => 'vega_wp_sanitize_text',

    ]);
    $wp_customize->add_control('3points_3_button_text', [
        'label' => __('Text akčního tlačítka'),
        'type' => 'text',
        'section' => 'vega_frontpage_3points'
    ]);
    $wp_customize->add_setting('3points_3_button_link', [
        'default' => $damenavas_defaults['3points_3_button_link'],
        'sanitize_callback' => 'vega_wp_sanitize_page',
    ]);
    $wp_customize->add_control('3points_3_button_link', [
        'label' => __('Odkaz akčního tlačítka'),
        'type' => 'dropdown-pages',
        'section' => 'vega_frontpage_3points'
    ]);

    $wp_customize->add_section('frontpage_news', [
        'title' => __('&nbsp;&nbsp;&nbsp;&nbsp;Aktuality'),
        'priority' => 2,
    ]);
    $wp_customize->add_setting('frontpage_news_category', [
        'default' => $damenavas_defaults['frontpage_news_category'],
        'sanitize_callback' => 'absint',
    ]);
    $wp_customize->add_control(new My_Dropdown_Category_Control($wp_customize,'frontpage_news_category', [
        'label' => __('Kategorie pro zobrazení na hlavní stránce'),
        'type' => 'dropdown-category',
        'section' => 'frontpage_news'
    ]));

    $wp_customize->add_section('frontpage_harmonogram', [
        'title' => __('&nbsp;&nbsp;&nbsp;&nbsp;Harmonogram', 'vega'),
        'priority' => 2,
    ]);
    $wp_customize->add_setting('frontpage_harmonogram_title', [
        'default' => $damenavas_defaults['frontpage_harmonogram_title'],
        'sanitize_callback' => 'vega_wp_sanitize_text',
    ]);
    $wp_customize->add_control('frontpage_harmonogram_title', [
        'label' => __('Nadpis'),
        'type' => 'text',
        'section' => 'frontpage_harmonogram'
    ]);
    $wp_customize->add_setting('frontpage_harmonogram_subtitle', [
        'default' => $damenavas_defaults['frontpage_harmonogram_subtitle'],
        'sanitize_callback' => 'vega_wp_sanitize_html',
    ]);
    $wp_customize->add_control(new RichText_Editor_Custom_Control($wp_customize, 'frontpage_harmonogram_subtitle', [
        'label' => __('Pod-nadpis'),
        'type' => 'textarea',
        'section' => 'frontpage_harmonogram'
    ]));
    $wp_customize->add_setting('frontpage_harmonogram_text', [
        'default' => $damenavas_defaults['frontpage_harmonogram_text'],
        'sanitize_callback' => 'vega_wp_sanitize_html',
    ]);
    $wp_customize->add_control(new RichText_Editor_Custom_Control($wp_customize, 'frontpage_harmonogram_text', [
        'label' => __('Text'),
        'type' => 'textarea',
        'section' => 'frontpage_harmonogram'
    ]));
    $wp_customize->add_setting('frontpage_harmonogram_button_text', [
        'default' => $damenavas_defaults['frontpage_harmonogram_button_text'],
        'sanitize_callback' => 'vega_wp_sanitize_text',
    ]);
    $wp_customize->add_control('frontpage_harmonogram_button_text', [
        'label' => __('Text akčního tlačítka'),
        'type' => 'text',
        'section' => 'frontpage_harmonogram'
    ]);
    $wp_customize->add_setting('frontpage_harmonogram_button_link', [
        'default' => $damenavas_defaults['frontpage_harmonogram_button_link'],
        'sanitize_callback' => 'vega_wp_sanitize_page',
    ]);
    $wp_customize->add_control('frontpage_harmonogram_button_link', [
        'label' => __('Odkaz akčního tlačítka'),
        'type' => 'dropdown-pages',
        'section' => 'frontpage_harmonogram'
    ]);

    $wp_customize->add_section('frontpage_happening_now', [
        'title' => __('&nbsp;&nbsp;&nbsp;&nbsp;Patička - Co se děje teď', 'vega'),
        'priority' => 2,
    ]);
    $wp_customize->add_setting('frontpage_happening_now_title', [
        'default' => $damenavas_defaults['frontpage_happening_now_title'],
        'sanitize_callback' => 'vega_wp_sanitize_text',
    ]);
    $wp_customize->add_control('frontpage_happening_now_title', [
        'label' => __('Nadpis'),
        'type' => 'text',
        'section' => 'frontpage_happening_now'
    ]);
    $wp_customize->add_setting('frontpage_happening_now_text', [
        'default' => $damenavas_defaults['frontpage_happening_now_text'],
        'sanitize_callback' => 'vega_wp_sanitize_text',
    ]);
    $wp_customize->add_control('frontpage_happening_now_text', [
        'label' => __('Text'),
        'type' => 'textarea',
        'section' => 'frontpage_happening_now'
    ]);
    $wp_customize->add_setting('frontpage_happening_now_button_title', [
        'default' => $damenavas_defaults['frontpage_happening_now_button_title'],
        'sanitize_callback' => 'vega_wp_sanitize_text',
    ]);
    $wp_customize->add_control('frontpage_happening_now_button_title', [
        'label' => __('Text akčního tlačítka'),
        'type' => 'text',
        'section' => 'frontpage_happening_now'
    ]);
    $wp_customize->add_setting('frontpage_happening_now_button_link', [
        'default' => $damenavas_defaults['frontpage_happening_now_button_link'],
        'sanitize_callback' => 'vega_wp_sanitize_page',
    ]);
    $wp_customize->add_control('frontpage_happening_now_button_link', [
        'label' => __('Odkaz akčního tlačítka'),
        'type' => 'dropdown-pages',
        'section' => 'frontpage_happening_now'
    ]);

    $wp_customize->add_section('footer', [
        'title' => __('&nbsp;&nbsp;&nbsp;&nbsp;Patička - logo a kontaktní údaje'),
        'priority' => 3,
    ]);
    $wp_customize->add_setting('footer_custom_text', [
        'sanitize_callback' => 'vega_wp_sanitize_html',
        'default' => $damenavas_defaults['footer_custom_text']
    ]);
    $wp_customize->add_control(new RichText_Editor_Custom_Control($wp_customize, 'footer_custom_text', [
        'label' => __('Vlastní text v patičce (např. kontaktní údaje)'),
        'type' => 'textarea',
        'section' => 'footer'
    ]));
    $wp_customize->add_setting('footer_custom_logo_image', [
        'sanitize_callback' => 'vega_wp_sanitize_text',
        'default' => $damenavas_defaults['footer_custom_logo_image'],
    ]);
    $wp_customize->add_control(new WP_Customize_Media_Control($wp_customize,'footer_custom_logo_image', [
        'type' => 'media',
        'section' => 'footer',
        'mime_type' => 'image',
        'label' => __('Logo města nebo organizace v patičce')
    ]));
    $wp_customize->add_setting('footer_custom_logo_link', [
        'sanitize_callback' => 'vega_wp_sanitize_url',
        'default' => $damenavas_defaults['footer_custom_logo_link']
    ]);
    $wp_customize->add_control('footer_custom_logo_link', [
        'type' => 'text',
        'label' => __('Odkaz k otevření při kliknutí na logo v patičce'),
        'section' => 'footer'
    ]);
}

add_action('customize_register', 'vega_wp_customize_register');


/*** Sanitize ***/

function vega_wp_sanitize_html($input)
{
    return wp_kses_post(force_balance_tags($input));
}

function vega_wp_sanitize_text($input)
{
    return sanitize_text_field($input);
}

function vega_wp_sanitize_radio_yn($input)
{
    $valid = array(
        'Y' => 'Yes',
        'N' => 'No'
    );
    if (array_key_exists($input, $valid)) {
        return $input;
    } else {
        return 'Y';
    }
}

function vega_wp_sanitize_radio_frontpage_banner($input)
{
    $valid = array(
        'Full Screen' => 'Full Screen',
        'Banner' => 'Banner'
    );
    if (array_key_exists($input, $valid)) {
        return $input;
    } else {
        return 'Banner';
    }
}

function vega_wp_sanitize_radio_colors($input)
{
    $valid = array(
        'Green' => 'Green',
        'Orange' => 'Orange',
        'Blue' => 'Blue'
    );
    if (array_key_exists($input, $valid)) {
        return $input;
    } else {
        return 'Orange';
    }
}

function vega_wp_sanitize_radio_blog_feed_display($input)
{
    $valid = array(
        'Small Image Left, Excerpt Right' => 'Small Image Left, Excerpt Right',
        'Large Image Top, Full Content Below' => 'Large Image Top, Full Content Below',
        'No Image, Excerpt' => 'No Image, Excerpt'
    );
    if (array_key_exists($input, $valid)) {
        return $input;
    } else {
        return 'Small Image Left, Excerpt Right';
    }
}

function vega_wp_sanitize_radio_banner($input)
{
    $valid = array(
        'Image Banner' => 'Image Banner',
        'Full Screen Image' => 'Full Screen Image',
        'Custom Header' => 'Custom Header',
        'None' => 'None',
        'Featured Image' => 'Featured Image'
    );
    if (array_key_exists($input, $valid)) {
        return $input;
    } else {
        return 'None';
    }

}

function vega_wp_sanitize_number($input)
{
    $input = (int)$input;
    $input = absint($input);
    if (is_int($input))
        return $input;
    else
        return '0';
}

function vega_wp_sanitize_page($input)
{
    $input = (int)$input;
    $input = absint($input);
    if (is_int($input))
        return $input;
    else
        return '0';
}

function vega_wp_sanitize_url($input): string
{
    return esc_url_raw($input);
}

function vega_wp_sanitize_html_class($input): string
{
    return sanitize_html_class($input);
}

?>