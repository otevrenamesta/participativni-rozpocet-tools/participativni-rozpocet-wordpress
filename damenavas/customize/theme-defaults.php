<?php

global $damenavas_defaults;

$damenavas_defaults['frontpage_text_1'] = 'Hlavní nadpis';
$damenavas_defaults['frontpage_text_2'] = 'Podnadpis';
$damenavas_defaults['frontpage_text_3'] = 'Poslední řádek';
$damenavas_defaults['frontpage_link'] = '/galerie-projektu/';

$damenavas_defaults['frontpage_text_4'] = 'GALERIE PROJEKTŮ';
$damenavas_defaults['frontpage_link'] = '/galerie-projektu';

$damenavas_defaults['show_image_slider'] = true;
$damenavas_defaults['show_hero_section'] = true;
$damenavas_defaults['show_3points'] = true;
$damenavas_defaults['show_news_section'] = true;
$damenavas_defaults['show_happening_now'] = true;
$damenavas_defaults['show_harmonogram'] = true;
$damenavas_defaults['show_mail_subscribe'] = true;

$damenavas_defaults['3points_1_title'] = 'Nadpis 1';
$damenavas_defaults['3points_1_text'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at elementum lectus. Sed vel risus et turpis pulvinar pretium. Duis quam tellus, pellentesque nec nisi ut, vestibulum pellentesque urna. Nullam quis tellus venenatis, commodo massa sed, gravida odio. Duis et eros massa. Sed non metus eu turpis lobortis maximus in eu tortor. Suspendisse id tellus turpis. In cursus nec massa a varius. Nullam vitae sem porttitor, porta justo eget, hendrerit elit. Phasellus orci diam, porttitor et libero sit amet, egestas semper odio. Maecenas vestibulum dolor vitae velit blandit efficitur.';
$damenavas_defaults['3points_1_button_link'] = '';
$damenavas_defaults['3points_1_button_text'] = 'PŘIHLÁSIT PROJEKT';
$damenavas_defaults['3points_2_title'] = 'Nadpis 1';
$damenavas_defaults['3points_2_text'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at elementum lectus. Sed vel risus et turpis pulvinar pretium. Duis quam tellus, pellentesque nec nisi ut, vestibulum pellentesque urna. Nullam quis tellus venenatis, commodo massa sed, gravida odio. Duis et eros massa. Sed non metus eu turpis lobortis maximus in eu tortor. Suspendisse id tellus turpis. In cursus nec massa a varius. Nullam vitae sem porttitor, porta justo eget, hendrerit elit. Phasellus orci diam, porttitor et libero sit amet, egestas semper odio. Maecenas vestibulum dolor vitae velit blandit efficitur.';
$damenavas_defaults['3points_2_button_link'] = '';
$damenavas_defaults['3points_2_button_text'] = 'JAK PODPOŘIT';
$damenavas_defaults['3points_3_title'] = 'Nadpis 1';
$damenavas_defaults['3points_3_text'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at elementum lectus. Sed vel risus et turpis pulvinar pretium. Duis quam tellus, pellentesque nec nisi ut, vestibulum pellentesque urna. Nullam quis tellus venenatis, commodo massa sed, gravida odio. Duis et eros massa. Sed non metus eu turpis lobortis maximus in eu tortor. Suspendisse id tellus turpis. In cursus nec massa a varius. Nullam vitae sem porttitor, porta justo eget, hendrerit elit. Phasellus orci diam, porttitor et libero sit amet, egestas semper odio. Maecenas vestibulum dolor vitae velit blandit efficitur.';$damenavas_defaults['3points_1_button_link'] = '';
$damenavas_defaults['3points_3_button_link'] = '';
$damenavas_defaults['3points_3_button_text'] = 'JAK ROZHODNOUT';

$damenavas_defaults['frontpage_news_category'] = '';

$damenavas_defaults['frontpage_harmonogram_title'] = 'Harmonogram nadpis';
$damenavas_defaults['frontpage_harmonogram_subtitle'] = 'Harmonogram podnadpis';
$damenavas_defaults['frontpage_harmonogram_text'] = 'Harmonogram text';
$damenavas_defaults['frontpage_harmonogram_button_text'] = 'Text tlačítka';
$damenavas_defaults['frontpage_harmonogram_button_link'] = '';

$damenavas_defaults['frontpage_happening_now_title'] = 'Co se děje teď?';
$damenavas_defaults['frontpage_happening_now_text'] = 'Myslíme na vás!';
$damenavas_defaults['frontpage_happening_now_button_title'] = 'GALERIE';
$damenavas_defaults['frontpage_happening_now_button_link'] = '';

$damenavas_defaults['footer_custom_text'] = '';
$damenavas_defaults['footer_custom_logo_image'] = '';
$damenavas_defaults['footer_custom_logo_link'] = '';

?>