<?php
global $damenavas_defaults;
?>
<!-- ========== Footer Nav and Copyright ========== -->
<div class="footer fifth-section">
    <div class="container-fluid">
        <?php if (is_home() && get_theme_mod('show_maiL_subscribe', $damenavas_defaults['show_mail_subscribe'])) { ?>
            <div class="row footer-input-row" style="background: #ebebeb;">
                <form id="footer-email" action="" method="post">
                    <div class="col-xs-12 form-group ">
                        <input class="footer-text-input" type="text" name="email" id="email"
                               placeholder="Dostávejte novinky přímo do e-mailu">
                        <input class="footer-submit" type="submit" value="přihlásit">
                    </div>
                </form>
                <div class="hidden col-xs-12 footer-submit-error">Zadal(a) jste špatný formát emailové adresy.</div>
            </div>
        <?php } ?>
        <div class="row my-footer-menu footer-column">
            <div class="col-sm-2 col-xs-12"></div>
            <div class="col-sm-2 col-xs-6">
                <?php
                wp_nav_menu(array(
                    'menu' => 'footer1'
                ));
                ?>
            </div>
            <div class="col-sm-2 col-xs-6">
                <?php
                wp_nav_menu(array(
                    'menu' => 'footer2'
                ));
                ?>
            </div>
            <div class="col-sm-2 col-xs-6">
                <?= get_theme_mod('footer_custom_text', $damenavas_defaults['footer_custom_text']) ?>
            </div>
            <div class="col-sm-2 col-xs-6">
                <div class="col-footer-block-image">
                    <?php if ($custom_logo_image = get_theme_mod('footer_custom_logo_image')): ?>
                        <?php if ($custom_logo_link = get_theme_mod('footer_custom_logo_link')): ?>
                            <a href="<?= $custom_logo_link ?>" target="_blank">
                        <?php endif; ?>
                        <div class="row footer-image-block">
                            <div class="footer-logo-brno"
                                 style="">
                                <?= wp_get_attachment_image($custom_logo_image, 'original') ?>
                            </div>
                        </div>
                        <?php if ($custom_logo_link): ?>
                            </a>
                        <?php endif; ?>
                    <?php endif; ?>
                    <div class="row footer-social-icon">
                        <!--<a href="https://www.facebook.com/damenavas" target="_blank">
                            <img src="/wp-content/themes/vega/assets/img/facebook.png">
                        </a>-->
                    </div>
                </div>
            </div>
            <div class="col-sm-2 col-xs-12"></div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {
        var errorFirst = true;
        jQuery("#footer-email").submit(function () {

            var email = jQuery("#email").val();

            if (!isValidEmailAddress(email)) {
                jQuery(".footer-submit-error").removeClass('hidden');

                errorFirst = false;

                if (!errorFirst) {
                    //jQuery(".footer-submit-error").effect( "bounce", {times:3}, 300 );
                }
                return false;
            }
        });
    });

    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    };
</script>

<!-- ========== /Footer Nav and Copyright ========== -->

<?php get_template_part('parts/footer', 'back-to-top'); ?>
<?php wp_footer(); ?>

</body>
</html>