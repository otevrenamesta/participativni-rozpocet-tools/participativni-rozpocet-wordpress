<?php /* Template Name: gallery-template */ ?>

<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>

    <div class="section page-content" style="margin-top: <?= is_admin_bar_showing() ? '120px' : '90px' ?>">
        <div id="page-<?php the_ID(); ?>">

            <?php the_content(); ?>

        </div>
    </div>

<?php endwhile; // end of the loop.  ?>
<?php get_footer(); ?>