<?php
/**
 * The template for displaying pages
 *
 * @package vega
 */
?>
<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    global $post;
    $size = "full";
    $imageUrl = get_the_post_thumbnail_url($post, $size);
    ?>
    <div class="container-fluid page-image-container">
        <div class="row page-image-row">
            <div class="col-sm-5 col-sm-offset-2 title">
                <h1><?php echo get_the_title(); ?></h1>
            </div>
            <?php if ($imageUrl != "") { ?>
                <div class="col-sm-5 image" style="/*background: url(<?php echo($imageUrl); ?>);*/">
                    <img src="<?php echo($imageUrl); ?>" class="hidden-xs"/>
                </div>
            <?php } ?>
        </div>
    </div>

    <!-- ========== Page Content ========== -->
    <div class="section page-content bg-white">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 p-page-content">
                    <div id="page-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>

                        <div class="page-content">
                            <?php the_content(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ========== /Page Content ========== -->

<?php endwhile; ?>

<?php get_footer(); ?>