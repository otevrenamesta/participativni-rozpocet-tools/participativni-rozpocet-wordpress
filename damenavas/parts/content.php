<?php
/**
 * The template part for displaying the post entry in the blog feed
 *
 * @package vega
 */
?>
<?php
$vega_wp_enable_demo = vega_wp_get_option('vega_wp_enable_demo');
$vega_wp_blog_feed_animations = vega_wp_get_option('vega_wp_blog_feed_animations');
$vega_wp_animations = vega_wp_get_option('vega_wp_animations');

?>

<?php 
	if($vega_wp_blog_feed_animations == 'Y' && $vega_wp_animations == 'Y') { $post_class = 'wow zoomIn'; }
	else { $post_class = ''; }
?>

<!-- Post -->
<div id="post-<?php the_ID(); ?>" <?php post_class('entry clearfix ' . $post_class); ?>>
    
    
    <?php #if no title is defined for the post...
    if(get_the_title() == '') { ?>
    
    <?php $id = get_the_ID(); ?>
    <?php if($vega_wp_blog_feed_display != 'Small Image Left, Excerpt Right') { ?>
    <!-- Post Title -->
    <h3 class="entry-title block-title block-title-left"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php _e('Post ID: ', 'vega'); echo $id; ?></a></h3>
    <?php } else { ?>
    <h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php _e('Post ID: ', 'vega'); echo $id; ?></a></h3>
    <?php } ?>
    <!-- /Post Title -->
    
    <?php } else { ?>

    <!-- Post Title -->
    <h3 class="entry-title block-title block-title-left"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

    <?php } ?>

    <!-- Large Image Top, Full Content Below -->
    <?php if(has_post_thumbnail()) { ?>
    <div class="entry-image entry-image-top">
        <a class="post-thumbnail post-thumbnail-large" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full', array( 'alt' => get_the_title() ) ); ?></a>
    </div>
    <?php } else if($vega_wp_enable_demo == 'Y') { ?>
        <a class="post-thumbnail post-thumbnail-large" href="<?php the_permalink(); ?>"><img src="<?php vega_wp_random_thumbnail('full'); ?>" class="img-responsive" /></a><?php } ?>  
    <div class="entry-content">        
        <?php the_content(__('View full post...', 'vega')); ?>
        <?php wp_link_pages(); ?>
    </div>
    <!-- /Large Image Top, Full Content Below -->

</div>

<!-- /Post -->