<?php
/**
* The template part for displaying the post entry in the recent posts on the front page (static)
*
* @package vega
*/
?>
<?php
global $key;
?>
<div class="post-grid recent-entry" id="recent-post-<?php the_ID(); ?>">
    <div class="recent-entry-image image">
        <?php if(has_post_thumbnail()) { ?>
        <a class="post-thumbnail post-thumbnail-recent" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'vega-post-thumbnail-recent', array( 'alt' => get_the_title(), 'class'=>'img-responsive' ) ); ?></a>
        <?php } else { ?>
        <a class="post-thumbnail post-thumbnail-recent" href="<?php the_permalink(); ?>"><img src="<?php vega_wp_random_thumbnail('vega-post-thumbnail-recent'); ?>" class="img-responsive" /></a><?php } ?>       
        <div class="caption">
            <div class="caption-inner">
                <a href="<?php the_permalink(); ?>" class="icon-link white"><i class="fa fa-link"></i></a>
            </div>
            <div class="helper"></div>
        </div>
    </div>
    <!-- Post Title -->
    <?php #if no title is defined for the post...
    if(get_the_title() == '') { $id = get_the_ID(); ?>
    <h4 class="recent-entry-title"><a href="<?php the_permalink(); ?>"><?php _e('ID: ', 'vega'); echo $id; ?></a></h4>
    <?php } else { ?>
    <h4 class="recent-entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
    <?php } ?>
    <!-- /Post Title -->
    
    <div class="recent-entry-content">
        <?= the_excerpt(); ?>
    </div>
    
</div>
