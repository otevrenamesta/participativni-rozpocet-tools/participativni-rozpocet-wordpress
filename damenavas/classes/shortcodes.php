<?php

function render_template(string $absolute_path, array $view_vars = [])
{
    ob_start();
    include($absolute_path);
    return ob_get_clean();
}

require_once get_template_directory() . '/classes/map-gallery.php';
require_once get_template_directory() . '/classes/projects-gallery.php';
require_once get_template_directory() . '/classes/project-detail.php';
require_once get_template_directory() . '/classes/projects-in-progress.php';
require_once get_template_directory() . '/classes/winner-projects.php';
require_once get_template_directory() . '/classes/propose-projects.php';

add_shortcode('IQ-Gallery-Maps', 'renderMaps');
add_shortcode('IQ-Projects-Gallery', 'renderGallery');
add_shortcode('IQ-Project-Detail', 'projectDetail');
add_shortcode('IQ-Realizace-Projektu', 'projectsInProgress');
add_shortcode('IQ-Vysledky-Hlasovani', 'winnerProjects');
add_shortcode('IQ-Propose-Projects-Banner', 'proposeProjectsBanner');
add_shortcode('IQ-Propose-Projects', 'proposeProjectsPage');