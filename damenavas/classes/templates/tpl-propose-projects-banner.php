<?php

?>

<div class="row paro2-propose-projects-banner">
    <div class="col-md-5 col col-md-offset-2 col-xs-offset-2">
        <h2>Navrhněte svůj vlastní projekt</h2>
    </div>
    <div class="col-md-5 col col-xs-offset-2">
        <br/>
        <a href="/moje-projekty/" class="btn-button">Pojďme na to!</a>
    </div>
</div>
