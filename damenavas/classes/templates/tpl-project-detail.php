<?php
$project = json_decode($view_vars['project'] ?? '{}', JSON_OBJECT_AS_ARRAY);
$project = array_shift($project);
$api_url = get_theme_mod('api_url');

wp_enqueue_script('mapycz-loader', 'https://api.mapy.cz/loader.js');
wp_enqueue_script('damenavas-project-detail', get_template_directory_uri() . '/assets/js/paro2-project-detail.js?'.rand(), ['jquery', 'damenavas-paro2']);
?>
<div class="project">
    <div class="container-fluid project-background-color project-header">
        <div class="row">
            <div class="col-xs-12 col-sm-offset-2 col-sm-5">
                <div class="project-title font-extra-bold project-font-color">
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 project-header-image">
                <img src="<?= (!empty($project) && !empty($project['image'])) ? $api_url . $project['image'] : '' ?>"
                     style="max-height: 350px; max-width: 500px;">
            </div>
        </div>
        <div class="row project-header-button">
            <div class="col-xs-12 col-sm-offset-2 col-sm-5">
                <button id="project-detail-like" class="project-background-color-invert project-font-color-invert text-uppercase" data-toggle="modal"
                        data-target="#voteModal" style="padding: 0.5rem 1rem; margin-top: 1rem;"><i
                            class="fa fa-2x fa-thumbs-up" aria-hidden="true"></i> líbí se mi
                </button>
                <button id="project-detail-dislike" class="project-background-color-invert project-font-color-invert text-uppercase" data-toggle="modal"
                        data-target="#voteModal" style="padding: 0.5rem 1rem; margin-top: 1rem;"><i
                            class="fa fa-2x fa-thumbs-down" aria-hidden="true"></i> nelíbí se mi
                </button>
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function () {
            jQuery('.button-scroll-to').click(function () {
                var scrollTo = '#scroll-' + jQuery(this).data('scroll');
                var elOffsetTop = jQuery(scrollTo).offset().top - 90;

                jQuery('html, body').animate({
                    scrollTop: (elOffsetTop)
                }, 1000);
            });
        });
    </script>
    <style>
        .project-info .info-panel i.fa {
            margin-right: 20px;
        }
    </style>

    <div class="container-fluid bg-grey mb-2 pt-1 pb-1 project-info">
        <div class="row info-panel mb-1">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="row">
                    <div class="col-sm-4 col-xs-12 mb-1 info-item">
                        <i class="project-font-color fa fa-2x fa-user"
                           aria-hidden="true"></i><span class="project-proposer-name"></span>
                    </div>
                    <div class="col-sm-4 col-xs-12 mb-1 info-item">
                        <i class="project-font-color fa fa-2x fa-money"
                           aria-hidden="true"></i>
                        <span class="project-price-proposed"></span>
                    </div>
                    <div class="col-sm-4 col-xs-12 mb-1 info-item">
                        <i class="project-font-color fa fa-2x fa-money"
                           aria-hidden="true"></i>
                        <span class="project-price-approved"></span>
                    </div>
                    <div class="col-sm-4 col-xs-12 mb-1 info-item">
                        <i class="project-font-color fa fa-2x fa-tag"
                           aria-hidden="true"></i>
                        <span class="project-categories"></span>
                    </div>
                    <div class="col-sm-4 col-xs-12 mb-1 info-item font-xs project-district-info-container">
                        <i class="project-font-color fa fa-2x fa-map-marker" aria-hidden="true"></i>
                        <span class="project-district-name"></span>
                    </div>
                    <div class="col-sm-4 col-xs-12 mb-1 info-item">
                        <i class="project-font-color fa fa-2x fa-calendar" aria-hidden="true"></i>
                        <span class="project-appeal-year"></span>
                    </div>
                    <div class="col-sm-4 col-xs-12 mb-1 info-item">
                        <i class="project-font-color fa fa-2x fa-thumbs-up" aria-hidden="true"></i>
                        <span id="block-like-count" class="project-like-count"></span> líbí se mi
                    </div>
                </div>
            </div>
        </div>
        <div class="row info-panel">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 info-panel-buttons">
                <button class="project-background-color project-font-color info-button font-bold text-uppercase button-scroll-to"
                        data-scroll="budget">Rozpočet projektu
                </button>
                <button class="paro2-show-map-button project-background-color project-font-color info-button font-bold text-uppercase button-scroll-to"
                        data-scroll="map">Zobrazit na mapě
                </button>
            </div>
        </div>
    </div>

    <div class="container-fluid mb-2">
        <div class="row">
            <div class="col-sm-5 col-sm-offset-2">
                <div class="project-annotation" style="font-weight: bold;">

                </div>
                <br/>
                <div class="project-description">

                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="row">
                    <div class="my-gallery project-image" itemscope itemtype="http://schema.org/ImageGallery">
                        <!--<figure class="col-xs-6" itemprop="associatedMedia" itemscope
                                itemtype="http://schema.org/ImageObject">
                            <a href="https://damenavas.brno.cz/wp-content/uploads/2021/03/IMG_20210302_065646-1.png"
                               itemprop="contentUrl" data-size="720 x 556">
                                <div style="background-image: url('https://damenavas.brno.cz/wp-content/uploads/2021/03/IMG_20210302_065646-1-300x232.png');"
                                     itemprop="thumbnail"></div>
                            </a>
                        </figure>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-red font-white pt-1 pb-1 text-center mb-2">
        <div class="row">
            <div class="col-xs-12 col-sm-offset-2 col-sm-8">
                <p class="project-public-interest">
                    <strong>VEŘEJNÝ PŘÍNOS PROJEKTU:</strong>
                    <span id="project-public-interest-content"></span>
                </p>
            </div>
        </div>
    </div>

    <div id="scroll-map" class="container-fluid mb-2">
        <div class="row">
            <div class="col-xs-12" id="project-detail-map" style="height: 300px; width: 100%;">
            </div>
        </div>
    </div>

    <div class="container-fluid mb-2">
        <div class="col-xs-12">
            <h3 class="font-extra-bold text-center mt-0">Deník projektu </h3>
        </div>
        <div class="col-xs-12 col-sm-offset-2 col-sm-8">
            <div id="project-block-diary" class="row" >
                <div class="col-xs-12">
                    <table class="table table-hover table-responsive mb-0">
                        <thead>
                        <tr>
                            <th style="min-width: 110px;">Datum</th>
                            <th>Popis</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br/><br/>


    <div id="scroll-budget" class="container-fluid mb-2">
        <div class="row">
            <div class="col-xs-12 col-sm-offset-2 col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="font-extra-bold text-center mt-0">Rozpočet projektu - návrh </h3>
                    </div>
                </div>
                <div id="project-block-budget" class="row">
                    <div class="col-xs-12">
                        <table class="table table-hover table-responsive mb-0" id="project-budget-proposed-items">
                            <thead>
                            <tr>
                                <th class="text-left">Popis položky</th>
                                <th class="text-center">Počet jednotek</th>
                                <th class="text-right">Cena za jednotku</th>
                                <th class="text-right">Cena celkem</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td> Cena celkem:</td>
                                <td colspan="4" class="price text-right" id="project-budget-proposed-total">600 000 Kč</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="scroll-budget-final" class="container-fluid mb-2">
        <div class="row">
            <div class="col-xs-12 col-sm-offset-2 col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="font-extra-bold text-center mt-0">Finální rozpočet projektu</h3>
                    </div>
                </div>
                <div id="project-block-budget-final" class="row">
                    <div class="col-xs-12">
                        <table class="table table-hover table-responsive mb-0" id="project-budget-approved-items">
                            <thead>
                            <tr>
                                <th class="text-left">Popis položky</th>
                                <th class="text-center">Počet jednotek</th>
                                <th class="text-right">Cena za jednotku</th>
                                <th class="text-right">Cena celkem</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td> Cena celkem:</td>
                                <td colspan="4" class="price text-right" id="project-budget-approved-total">600 000 Kč</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="scroll-feasibility" class="container-fluid mb-2">
        <div class="row">
            <div class="col-xs-12 col-sm-offset-2 col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="font-extra-bold text-center mt-0">Posouzení proveditelnosti</h3>
                    </div>
                </div>
                <div id="project-block-feasibility" class="row">
                    <div class="col-xs-12">
                        <table class="table table-hover table-responsive mb-0" id="project-feasibility-items">
                            <thead>
                            <tr>
                                <th class="text-left">Subjekt</th>
                                <th class="text-center">Odůvodnění</th>
                                <th class="text-right">Závěr</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>