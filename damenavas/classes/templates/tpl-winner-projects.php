<?php
wp_enqueue_script('damenavas-winners', get_template_directory_uri() . '/assets/js/paro2-winner-projects.js?' . rand(), ['jquery', 'damenavas-paro2']);

?>
<div class="container-fluid">
    <div id="voteApplicationBlock" style="">
        <div class="row">
            <div class="col-xs-11 col-xs-offset-1 col-md-5 col-md-offset-2">
                <h1 class="vap-title"><?= __('Výsledky hlasování') ?></h1>
                <p class="vap-perex">
                    Seznamte se s konečným pořadím projektů. <br>
                    Všechny projekty, které jsou nad čarou, budou realizovány.<br>
                    Pořadí projektů určuje počet dosažených <span style="color: #6aa845; font-weight: 700">kladných hlasů.</span><br>
                    <b>Děkujeme za vaše hlasy.</b>
                </p>
            </div>
            <div class="col-md-4 col-xs-11 col-xs-offset-1 g-stats-block">
                <div class="g-stats-row">
                    <div class="g-stats-number paro2-total-shown-votes" style="text-align: right; margin-right: 10px">

                    </div>
                    <div class="g-stats-label" style="min-width: 130px">
                        hlasů
                    </div>
                </div>
            </div>
        </div>

        <div class="row gallery-block2">
            <form id="filterForm" action="" method="get">

                <div class="col-sm-12 col-md-2 gallery-filter-row-title vap-gallery-filter-row-title">
                    <span>Filtrovat:</span>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-2">
                    <select id="years" class="vap-gallery-filter-select">
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 realization-gallery-flex">
                </div>
            </form>
        </div>

        <br/>

        <div class="row">
            <div class="col-xs-12">
                <div class="projects" id="projects-in-progress-container">

                </div>
            </div>
        </div>

        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="pswp__bg"></div>
            <div class="pswp__scroll-wrap">
                <div class="pswp__container">
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                </div>
                <div class="pswp__ui pswp__ui--hidden">
                    <div class="pswp__top-bar">
                        <div class="pswp__counter"></div>
                        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                        <button class="pswp__button pswp__button--share" title="Share"></button>
                        <div class="pswp__preloader">
                            <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                        <div class="pswp__share-tooltip"></div>
                    </div>
                    <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                    </button>
                    <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                    </button>
                    <div class="pswp__caption">
                        <div class="pswp__caption__center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>