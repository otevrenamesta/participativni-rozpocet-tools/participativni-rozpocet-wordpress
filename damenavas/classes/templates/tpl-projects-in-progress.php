<?php
wp_enqueue_script('percircle', get_template_directory_uri() . '/assets/js/percircle.js', ['jquery']);
wp_enqueue_script('damenavas-in-progress', get_template_directory_uri() . '/assets/js/paro2-projects-in-progress.js?' . rand(), ['jquery', 'damenavas-paro2', 'percircle']);

?>
<div class="container-fluid">
    <div id="voteApplicationBlock" style="">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-2">
                <h1 class="vap-title"><?= __('Realizace projektů') ?></h1>
                <p class="vap-perex">
                    <?= __('Vy jste navrhli, vy jste podpořili, vy jste rozhodli. A město Brno ty nejúspěšnější projekty právě připravuje.<br>Podívejte se, jak probíhá jejich realizace:') ?>
                </p>
            </div>
        </div>

        <div class="row gallery-block2">
            <form id="filterForm" action="" method="get">

                <div class="col-sm-12 col-md-2 gallery-filter-row-title vap-gallery-filter-row-title">
                    <span>Filtrovat:</span>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-2">
                    <select id="years" class="vap-gallery-filter-select">
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2">
                    <select id="district" class="vap-gallery-filter-select">
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2">
                    <select id="category" class="vap-gallery-filter-select">
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 realization-gallery-flex">
                </div>
            </form>
        </div>

        <div class="mb-1 mt-1 row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <button type="button" id="realization-gallery-show-all" class="realization-gallery-btn-gray">
                    Rozbalit vše
                </button>
                <button type="button" id="realization-gallery-hide-all" class="realization-gallery-btn-gray ml-1">
                    Skrýt vše
                </button>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="projects" id="projects-in-progress-container">

                </div>
            </div>
        </div>

        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="pswp__bg"></div>
            <div class="pswp__scroll-wrap">
                <div class="pswp__container">
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                </div>
                <div class="pswp__ui pswp__ui--hidden">
                    <div class="pswp__top-bar">
                        <div class="pswp__counter"></div>
                        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                        <button class="pswp__button pswp__button--share" title="Share"></button>
                        <div class="pswp__preloader">
                            <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                        <div class="pswp__share-tooltip"></div>
                    </div>
                    <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                    </button>
                    <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                    </button>
                    <div class="pswp__caption">
                        <div class="pswp__caption__center"></div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            jQuery(".realization-gallery-project").on("click", function () {
                if (jQuery(this).hasClass("project_open")) {
                    jQuery(this).removeClass("project_open");
                } else {
                    jQuery(".project").removeClass("project_open");
                    jQuery(this).addClass("project_open");
                }
            });

            jQuery('#realization-gallery-show-all').click(function () {
                jQuery('.realization-gallery-project').addClass("project_open");
            });

            jQuery('#realization-gallery-hide-all').click(function () {
                jQuery('.realization-gallery-project').removeClass("project_open");
            });

            function openSingleRealizationPhoto(img) {
                var height = img.getAttribute("height");
                var src = jQuery(img).data('fullsrc');
                var largestSize = img.getAttribute("width");
                var downloadLink = src;

                var items = [{
                    src: src,
                    w: largestSize,
                    h: height,
                    downloadLink: downloadLink
                }];
                var options = {
                    bgOpacity: 0.9,
                    fullscreenEl: false,
                    history: false,
                    shareButtons: [
                        {id: 'download', label: 'Stáhnout obrázek', url: "{{raw_image_url}}", download: true}
                    ]
                };
                var gallery = new PhotoSwipe(jQuery(".pswp")[0], PhotoSwipeUI_Default, items, options);
                gallery.init();
            }

            jQuery(document).on("click", '.image-open', function () {
                openSingleRealizationPhoto(this);
            });
        </script>
    </div>
</div>