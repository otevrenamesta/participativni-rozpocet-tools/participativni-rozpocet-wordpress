<?php
wp_enqueue_script('mapycz-loader', 'https://api.mapy.cz/loader.js');
wp_enqueue_script('damenavas-propose-projects', get_template_directory_uri() . '/assets/js/paro2-propose-projects.js?' . rand(), ['jquery', 'damenavas-paro2']);

?>
<div class="container-fluid">
    <div id="voteApplicationBlock" class="paro2-project-propose-container" style="">
        <div class="row">
            <div class="col-xs-11 col-xs-offset-1 col-md-5 col-md-offset-2">
                <h1 class="vap-title"><?= __('Navrhněte svůj vlastní projekt') ?></h1>
            </div>
            <div class="col-md-4 col-xs-11 col-xs-offset-1 pt-3">
                <div class="paro2-current-user">

                </div>
                <a class="btn-button paro2-red-button paro2-logout hidden">Odhlásit se</a>
            </div>
        </div>

        <br/>

        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <h3>Navrhovatel</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <label for="project-proposer-name">Vaše jméno</label>
                <input name="project-proposer-name" type="text" class="mt-1" maxlength="200" placeholder="" required="required">
            </div>
            <div class="col-xs-offset-2 col-xs-8">
                <label for="project-proposer-email">Váš e-mail</label>
                <input name="project-proposer-email" type="email" class="mt-1" maxlength="200" placeholder="" required="required">
            </div>
            <div class="col-xs-offset-2 col-xs-8">
                <label for="project-proposer-phone">Váš telefon</label>
                <input name="project-proposer-phone" type="tel" class="mt-1" maxlength="200" placeholder="">
            </div>
        </div>

        <hr/>

        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <h3>Základní informace o projektu</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <label for="project-name">Název projektu</label>
                <input name="project-name" type="text" class="mt-1" maxlength="200" placeholder="Název projektu" required="required">
            </div>
        </div>

        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <label for="project-category">Kategorie projektu</label>
                <select id="paro2-project-categories" name="project-category" type="text" class="mt-1" required="required">

                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <label for="project-annotation">Anotace (krátký popis)</label>
                <input name="project-annotation" type="text" class="mt-1" maxlength="200" placeholder="" required="required">
            </div>
        </div>

        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <label for="project-description">Popis projektu</label>
                <textarea name="project-description" type="text" class="mt-1" placeholder="" required="required"></textarea>
            </div>
        </div>

        <hr/>

        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <h3>Umístění projektu</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <input name="project-has-no-location" type="checkbox" class="mt-1">
                <label for="project-has-no-location" class="d-inline">Projekt není vázán na konkrétní lokalitu</label>
            </div>
        </div>

        <div class="row hide-without-location">
            <div class="col-xs-offset-2 col-xs-8">
                <label for="project-address">Adresa / Místo realizace projektu</label>
                <input name="project-address" type="text" class="mt-1" maxlength="200" placeholder="Kde se bude projekt realizovat" required="required">
            </div>
        </div>

        <div class="row hide-without-location">
            <div class="col-xs-offset-2 col-xs-8">
                <label for="project-district">Městská část / obvod</label>
                <select id="paro2-project-districts" name="project-district" type="text" class="mt-1" required="required">

                </select>
            </div>
        </div>

        <div class="row hide-without-location">
            <div class="col-xs-offset-2 col-xs-8">
                <h3>Umístění projektu na mapě</h3>
            </div>
        </div>

        <div class="row hide-without-location">
            <div class="col-xs-offset-2 col-xs-8" id="mapTarget" style="height: 250px;">

            </div>
        </div>


        <br/><br/>

    </div>
</div>