<?php
wp_enqueue_script('mapycz-loader', 'https://api.mapy.cz/loader.js');
wp_enqueue_script('damenavas-map', get_template_directory_uri() . '/assets/js/paro2-map.js?' . rand(), ['jquery', 'damenavas-paro2']);

?>

<div class="container-fluid gallery-block2">
    <form id="filterForm" action="" method="post">
        <div class="row bg-grey">
            <div class="col-sm-offset-2 col-sm-8 col-xs-12">
                <div class="row row-no-gutters pt-2 pb-2">
                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <button class="first show-filter btn filter-button pull-right" type="button"><i
                                    class="fa fa-filter font-white"></i>zobrazit
                            filtr
                        </button>
                        <button class="first bg-dark-grey reset-filter btn filter-button pull-right" type="button"
                                style="display: none;">
                            <i class="fa fa-times"></i>zrušit filtr
                        </button>
                    </div>
                    <div class="col-xs-8 col-sm-9 col-md-5">
                        <input type="text" name="project_name" placeholder="Vyhledat dle názvu projektu"
                               class="filter-search">
                    </div>
                    <div class="col-md-4 hidden-sm hidden-xs">
                        <a class="second btn filter-button" type="button" href="/galerie-projektu"><i
                                    class="fa fa-map-marker font-white"></i>zobrazit galerii projektů
                        </a>
                    </div>
                </div>
                <div id="filter-block" class="row" style="display: none;">
                    <div class="col-xs-12">
                        <div class="col-md-3 col-xs-12 districtFilter">
                            <select id="district" name="district" class="vap-gallery-filter-select">
                                <option value="all">dle městské části</option>
                            </select>
                        </div>

                        <div class="col-md-3 col-xs-12 categoryFilter">
                            <select id="category" name="category" class="vap-gallery-filter-select">
                                <option value="all">dle oblasti projektu</option>
                            </select>
                        </div>

                        <div class="col-md-3 col-xs-12 statusFilter">
                            <select id="status" name="status" class="vap-gallery-filter-select">
                                <option value="all">dle stavu projektu</option>
                            </select>
                        </div>

                        <div class="col-md-3 col-xs-12 yearFilter">
                            <select id="year" name="y" class="vap-gallery-filter-select">
                                <option value="all">Vše</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="row mb-2">
        <div class="col-sm-8 col-md-9">
            <div id="map-gallery"></div>
        </div>
        <div class="col-sm-4 col-md-3">
            <h3><?= __('Legenda - Stavy projektů') ?></h3>
            <ul id="map-legend">

            </ul>
        </div>
    </div>

</div>
