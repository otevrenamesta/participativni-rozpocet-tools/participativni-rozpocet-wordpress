<?php
function projectDetail()
{
    $api_url = get_theme_mod('api_url');
    $organization_id = get_theme_mod('api_organization_id');
    $project_id = intval($_GET['id'] ?? 0);
    $project_info_url = sprintf("%s/api/v1/organization/%d/project/%d/info.json", $api_url, $organization_id, $project_id);
    $project_info = null;

    try {
        $handle = curl_init($project_info_url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $project_info = curl_exec($handle);
        $http_status = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
        if ($http_status !== 200) {
            global $wp_query;
            $wp_query->set_404();
            status_header(404);
            nocache_headers();
            include(get_query_template('404'));
            return;
        }
    } catch (Throwable $e) {
    }

    return render_template(__DIR__ . '/templates/tpl-project-detail.php', [
        'project' => $project_info
    ]);
}

?>