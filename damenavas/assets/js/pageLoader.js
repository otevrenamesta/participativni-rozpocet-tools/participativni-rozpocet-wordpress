jQuery(document).on( "click", ".page-button", function() {

    var data = {
        'page': jQuery(this).data('page'),
        'pageNumber' : jQuery(this).data('curr'),
        'type' : jQuery(this).data('type')
    }
    
    // prihlaseni
    if(jQuery(this).data('curr') == 1 && jQuery(this).data('type') == 'next'){   
        var opNumber = jQuery(document).find('input[name="op_number"]').val();
        
        var birthDay = jQuery('#frm-birth_day').val();
        var birthMonth = jQuery('#frm-birth_month').val();
        var birthYear = jQuery('#frm-birth_year option:selected').val();

        var agreement = jQuery(document).find('input[name="personal_data_agreement"]').is(":checked");
       

        if (opNumber.length == 0){   
            jQuery('#alertModal #alert-message').html('Zadejte prosím číslo OP.');
            jQuery('#alertModal').modal('show');         
            return false;
        }

        if ((opNumber.length != 8 && opNumber.length != 9) || !/^\w+$/.test(opNumber)){  
            jQuery('#alertModal #alert-message').html('Číslo OP je zadáno ve špatném formátu!');
            jQuery('#alertModal').modal('show');
            return false;
        }

        if(!agreement){
            jQuery('#alertModal #alert-message').html('Potřebujeme váš souhlas se zpracováním osobních údajů.');
            jQuery('#alertModal').modal('show'); 
            return false;
        }

        var birthDate = new Date(birthYear + '-' + birthMonth + '-' + birthDay);
        var verifyDate = new Date(birthDate.getFullYear() + 18, birthDate.getMonth(), birthDate.getDate());
        
        if(verifyDate > new Date()){  
            jQuery('#alertModal #alert-message').html('Hlasovat mohou pouze osoby starší 18 let!');
            jQuery('#alertModal').modal('show');          
            return false;
        }
        
        data['formData'] = jQuery('form').serialize();    
      
    }

    // hlasovani
    if(jQuery(this).data('curr') == 3 && jQuery(this).data('type') == 'next'){
        data['vote'] = {'plus' : projectsPlus, 'minus' : projectsMinus};  
    }

    if((jQuery(this).data('curr') == 3 && jQuery(this).data('type') == 'next')){
        if(projectsPlus.length > 0 || projectsMinus.length > 0){
            getModal(data);
            return;
        }
    }

    if(jQuery(this).data('curr') == 4){
        data['questData'] = jQuery('form').serialize();   
    }
    
    getPage(data);
});

jQuery(document).on('click', '.save-project-button', function(){
    jQuery(".page-button").removeClass("page-button");
    jQuery('#confirmModal').modal('hide');

    var data = {
        'page': 'questionnaire',
        'pageNumber' : null,
        'action': 'save'
    }

    data['vote'] = {'plus' : projectsPlus, 'minus' : projectsMinus}; 

    jQuery(document).on('hidden.bs.modal', '#confirmModal', function(){
        getPage(data);
    });
});

jQuery(document).on('hidden.bs.modal', '#confirmModal', function(){
    jQuery(".next-button").addClass("page-button");
});

jQuery(document).on('hidden.bs.modal', '#alertModal', function(){
    jQuery(".vap-verify-button").addClass("page-button");
});

function getPage(data){
    jQuery("#loaderGif").show();
    jQuery(".page-button").removeClass("page-button");

    ajaxRequest = jQuery.ajax({
        url: "/wp-content/script/ajax/get-page.php",
        type: "post",
        data: data
    });

    ajaxRequest.done(function (response){

        grecaptcha.reset();

        response = JSON.parse(response);
        jQuery("#loaderGif").hide();
        if(response['html'] == "service_error"){  
            jQuery('#alertModal #alert-message').html('Hlasovací modul momentálně není dostupný. Vraťte se prosím později.');
            jQuery('#alertModal').modal('show');             
        } else if(response['html'] == "data_error"){ 
            jQuery('#alertModal #alert-message').html('Zadaná data neexistují v Registru obyvatel! Můžete je opravit a znovu odeslat.');
            jQuery('#alertModal').modal('show'); 
        } else if(response['html'] == "not-resident"){
            jQuery('#alertModal #alert-message').html('Dle zadaných údajů nejste obyvatelem Brna. V případě nesouhlasu můžete kontaktovat koordinátorku na adrese <a href="mailto:info@damenavas.brno.cz" style="color: white; font-weight: bold;">info@damenavas.brno.cz.');
            jQuery('#alertModal').modal('show'); 
        } else if(response['html'] == "under-18"){
            jQuery('#alertModal #alert-message').html('Hlasovat mohou pouze osoby starší 18 let!');
            jQuery('#alertModal').modal('show');
        } else if(response['html'] == "voted-offline"){
            jQuery('#alertModal #alert-message').html('Již jste hlasoval(a) papírovým hlasovacím lístkem prostřednictvím koordinátora!');
            jQuery('#alertModal').modal('show');
        } else if(response['html'] == "end-vote"){
            window.onbeforeunload = null;
            window.location.href = "./dekujeme";
        } else if(response['html'] == "g-empty"){
            jQuery('#alertModal #alert-message').html('Prosíme klikněte na reCAPTCHA box.');
            jQuery('#alertModal').modal('show');
        } else if(response['html'] == "g-error"){
            jQuery('#alertModal #alert-message').html('Vyplňte prosím ochranu proti robotům pod souhlasem se zpracováním osobních údajů.');
            jQuery('#alertModal').modal('show');
        } else {
            jQuery('#voteApplicationBlock').html(response['html']);
        }
    });
}

function getModal(data){
    jQuery("#loaderGif").show();

    ajaxRequest = jQuery.ajax({
        url: "/wp-content/script/ajax/get-confirm-modal.php",
        type: "post",
        data: data
    });

    ajaxRequest.done(function (response){
        jQuery("#loaderGif").hide();
        jQuery('.votesBlock').html(response);
        jQuery('#confirmModal').modal('show'); 
    });
}

