jQuery(document).ready(function(){
    jQuery('.custom-datepicker').datepicker({
        format: 'dd.mm.yyyy',
        language: "sk",
        todayBtn: 'linked',
    });
});