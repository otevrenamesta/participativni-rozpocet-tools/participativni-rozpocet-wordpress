;(function ($, window, document, undefined) {

    let mapGallery = {
        project_states_to_color: {},
        project_categories_to_color: {},
        contrast_colors: {},
        count_categories: {},
        count_states: {},
        count_districts: {},
        count_years: {},
        inProgressData: {},
        loadProjectsInProgress: function () {
            $.get(
                PARO_API_URL + '/api/v1/organization/' + PARO_API_ORGANIZATION_ID + '/projects_in_progress.json'
            ).done(function (data) {
                mapGallery.inProgressData = data;
                mapGallery.appealLoadedCallback();
            })
        },
        appealLoadedCallback: function () {
            // make counts for further selects filling
            mapGallery.count_districts = {};
            mapGallery.count_categories = {};
            mapGallery.count_states = {};
            mapGallery.count_years = {};

            for (const val of Object.values(mapGallery.inProgressData)) {
                if ('categories' in val) {
                    for (const key of Object.keys(val.categories)) {
                        mapGallery.count_categories[key] = (mapGallery.count_categories[key] || 0) + 1;
                    }
                }
                if ('district' in val && typeof val.district === 'object') {
                    mapGallery.count_districts[val.district.id] = (mapGallery.count_districts[val.district.id] || 0) + 1;
                }
                mapGallery.count_years[val.appeal_year] = 1 + (mapGallery.count_years[val.appeal_year] || 0);
            }

            if ('districts' in PARO_ORGANIZATION_INFO.organization) {
                let $districtsTarget = $("select#district");
                mapGallery.filter_district_target = $districtsTarget;
                $districtsTarget.on('change', mapGallery.filterRefresh);
                $districtsTarget.empty();
                $districtsTarget.append(new Option('dle městské části', 'all'));
                for (const [key, val] of Object.entries(PARO_ORGANIZATION_INFO.organization.districts)) {
                    let _count = (mapGallery.count_districts[key] || 0);
                    let _option = new Option(val + " (" + _count + ") ", key)
                    if (_count < 1) {
                        _option.disabled = true;
                    }
                    $districtsTarget.append(_option);
                }
            }

            let $categoriesTarget = $("select#category");
            mapGallery.filter_category_target = $categoriesTarget;
            $categoriesTarget.on('change', mapGallery.filterRefresh);
            $categoriesTarget.empty();
            $categoriesTarget.append(new Option('dle oblasti projektu', 'all'));
            for (const [key, val] of Object.entries(PARO_ORGANIZATION_INFO.organization.project_categories)) {
                let _count = (mapGallery.count_categories[key] || 0);
                let _option = new Option(val.name + " (" + _count + ") ", key)
                if (_count < 1) {
                    _option.disabled = true;
                }
                $categoriesTarget.append(_option);
                mapGallery.project_categories_to_color[parseInt(key)] = val.color;
                mapGallery.contrast_colors[val.color.toLowerCase()] = val.contrast_color;
            }


            let $yearsTarget = $("select#years");
            mapGallery.filter_year_target = $yearsTarget;
            $yearsTarget.on('change', mapGallery.filterRefresh);
            $yearsTarget.empty();
            $yearsTarget.append(new Option('Všechny ročníky', 'all'));
            for (const val of Object.values(PARO_ORGANIZATION_INFO.organization.appeals)) {
                $yearsTarget.append(new Option('Ročník ' + val.year, val.year));
            }

            mapGallery.filterRefresh();
        },
        filter_year_target: undefined,
        filter_year: undefined,
        filter_state_target: undefined,
        filter_state: undefined,
        filter_category_target: undefined,
        filter_category: undefined,
        filter_district_target: undefined,
        filter_district: undefined,
        filter_search_target: undefined,
        filter_search: undefined,
        filterRefresh: function (event) {
            let filterChanged = false;
            if (mapGallery.filter_year_target) {
                let current_year = parseInt(mapGallery.filter_year_target.val());
                if (mapGallery.filter_year !== current_year) {
                    mapGallery.filter_year = current_year;
                    filterChanged = true;
                }
            }
            if (mapGallery.filter_category_target) {
                let current_category = parseInt(mapGallery.filter_category_target.val());
                if (mapGallery.filter_category !== current_category) {
                    mapGallery.filter_category = current_category;
                    filterChanged = true;
                }
            }
            if (mapGallery.filter_district_target) {
                let current_district = parseInt(mapGallery.filter_district_target.val());
                if (mapGallery.filter_district !== current_district) {
                    mapGallery.filter_district = current_district;
                    filterChanged = true;
                }
            }
            if (filterChanged) {
                mapGallery.reloadProjects();
            }
        },
        reloadProjects: function () {
            let $projectsTarget = $("#projects-in-progress-container");
            for (const project of Object.values(mapGallery.inProgressData)) {
                let $projectContainer = $("#project-" + project.id, $projectsTarget);
                if (!$projectContainer.length) {
                    $projectContainer = $("<div class=\"projects__item realization-gallery-project\">\n" +
                        "                        <div class=\"project__left\">\n" +
                        "                            <div class=\"project__head\">\n" +
                        "                                <div class=\"project__head-top\">\n" +
                        "                                    <h2 class=\"project__title paro2-project-title\"></h2>\n" +
                        "                                    <div class=\"project__year paro2-appeal-year\"></div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"project__head-bottom\">\n" +
                        "                                    <div class=\"project__badge\">\n" +
                        "                                        <div class=\"project__icon\"><i class=\"fa fa-map-marker\"></i></div>\n" +
                        "                                        <span class=\"paro2-district-name\"></span>\n" +
                        "                                    </div>\n" +
                        "                                    <div class=\"project__badge\">\n" +
                        "                                        <div class=\"project__icon\">Kč</div>\n" +
                        "                                        <span class=\"paro2-budget-total\"></span>\n" +
                        "                                    </div>\n" +
                        "                                    <div class=\"project__badge\">\n" +
                        "                                        <div class=\"project__icon\"><i class=\"fa fa-tag\"></i></div>\n" +
                        "                                        <span class=\"paro2-categories\"></span>\n" +
                        "                                    </div>\n" +
                        "                                    <div class=\"project__more paro2-show-more\">VÍCE INFORMACÍ</div>\n" +
                        "                                </div>\n" +
                        "                            </div>\n" +
                        "                            <div class=\"project__body\">\n" +
                        "                                <div class=\"project__description\">\n" +
                        "                                    <p><strong>Aktualizace:</strong> <span class=\"paro2-last-update\"></span></p>\n" +
                        "                                </div>\n" +
                        "                                <a class=\"realization-gallery-btn paro2-open-project\" href=\"#\">DETAIL PROJEKTU</a>\n" +
                        "                            </div>\n" +
                        "                        </div>\n" +
                        "                        <div class=\"project__right\">\n" +
                        "                            <div class=\"project__votes\">\n" +
                        "                                <div class=\"project__votes-circle\">\n" +
                        "                                    <div class=\"project__votes-percircle\" data-percent=''>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"project__votes-total paro2-votes-oerall\"></div>\n" +
                        "                                <div class=\"project__votes-parts\">\n" +
                        "                                    <div class=\"project__votes-positive\"><span class=\"paro2-votes-positive\"></span> <i class=\"fa fa-plus-circle\"></i></div>\n" +
                        "                                    <div class=\"project__votes-negative\"><span class=\"paro2-votes-negative\"></span> <i class=\"fa fa-minus-circle\"></i></div>\n" +
                        "                                </div>\n" +
                        "                            </div>\n" +
                        "                            <div class=\"project__photos\">\n" +
                        "                                <div class=\"project__photo\">\n" +
                        "                                    <div class=\"project__photo-inner paro2-project-photo\"\n" +
                        "                                         data-fullsrc=\"\"\n" +
                        "                                         style=\"\"></div>\n" +
                        "                                </div>\n" +
                        "                            </div>\n" +
                        "                        </div>\n" +
                        "                    </div>");

                    $projectContainer.attr('id', 'project-' + project.id);
                    $(".paro2-project-title", $projectContainer).text(project.name);
                    $(".paro2-appeal-year", $projectContainer).text(project.appeal_year);
                    if (typeof (project.district || undefined) === 'object') {
                        $(".paro2-district-name", $projectContainer).text(project.district.name);
                    } else {
                        $(".paro2-district-name", $projectContainer).parent().toggle(false);
                    }
                    $(".paro2-budget-total", $projectContainer).text($.fn.paro2.formatCurrency(project.approved_price_total));
                    $(".paro2-categories", $projectContainer).text(Object.values(project.categories || []).join(', '));

                    $(".project__votes-percircle", $projectContainer).attr('data-percent', project.progress);
                    $(".paro2-show-more", $projectContainer).click(() => {
                        $projectContainer.toggleClass('project_open')
                    });
                    $(".paro2-open-project", $projectContainer).click(() => {
                        window.open('/projekt/?id=' + project.id, '_self');
                    });

                    $projectContainer.appendTo($projectsTarget);
                }
                let shouldShow = mapGallery.matchesFilter(project);
                $projectContainer.toggle(shouldShow);
            }
            $(".project__votes-percircle", $projectsTarget).percircle();
        },
        matchesFilter: function (project) {
            if (mapGallery.filter_year && parseInt(project.appeal_year) !== parseInt(mapGallery.filter_year)) {
                if (mapGallery.filter_year !== 'all') {
                    console.log('year: ' + parseInt(project.appeal_year) + " vs. " + parseInt(mapGallery.filter_year))
                    return false;
                }
            }
            if (mapGallery.filter_district && (!('district' in project) || parseInt(project.district.id) !== parseInt(mapGallery.filter_district))) {
                console.log('district: ' + ('district' in project ? parseInt(project.district.id) : 'N/A') + " vs. " + parseInt(mapGallery.filter_district))
                return false;
            }
            if (mapGallery.filter_category) {
                let expectedCategory = parseInt(mapGallery.filter_category);
                let foundMatching = false;
                for (const id of Object.keys(project.categories)) {
                    if (parseInt(id) === expectedCategory) {
                        foundMatching = true;
                        break;
                    }
                }
                if (!foundMatching) {
                    console.log('category not found: ' + parseInt(mapGallery.filter_category));
                    return false;
                }
            }
            if (mapGallery.filter_search && mapGallery.filter_search.trim().length >= 2) {
                let term = mapGallery.filter_search.trim().toLowerCase();
                if (!JSON.stringify(project).toLowerCase().includes(term)) {
                    return false;
                }
            }
            return true;
        },
        run: function () {
            console.log('projectsGallery run');

            // load organization
            $.fn.paro2.loadOrganization(mapGallery.loadProjectsInProgress)

            $(".show-filter").click(function () {
                $("#filter-block").show();
                $(this).hide();
                $(".reset-filter").show();
            });
            $(".reset-filter").click(function () {
                $("#filter-block").hide();
                $(this).hide();
                $(".show-filter").show();
            });
        }
    };

    $.fn.paro2.mapGallery = mapGallery;
    $.fn.paro2.mapGallery.run();

})(jQuery, window, document);