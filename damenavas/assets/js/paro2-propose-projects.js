;(function ($, window, document, undefined) {

    let projectDetail = {
        map: undefined,
        mapy_loaded: false,
        marker: undefined,
        map_target_selector: 'project',
        projects_layer: undefined,
        mapyLoadedCallback: function () {
            projectDetail.mapy_loaded = true;
            projectDetail.showProjectLocation();
        },
        projectLoadedCallback: function () {
            projectDetail.showProjectLocation();

            $("input, select, textarea", $(".paro2-project-propose-container")).change(function(){
                console.log("changed value", $(this).val(), $(this));
                if ($(this).attr('name') === 'project-has-no-location') {
                    $(".hide-without-location").toggle(!$(this).is(":checked"))
                }
            });

            let $categoriesTarget = $("#paro2-project-categories");
            $categoriesTarget.append(new Option(
                "Vyberte si prosím z možností"
            ));
            for (const [key, val] of Object.entries(PARO_ORGANIZATION_INFO.organization.project_categories)) {
                $categoriesTarget.append(new Option(
                    val.name,
                    parseInt(key),
                ));
            }

            let $districtsTarget = $("#paro2-project-districts");
            $districtsTarget.append(new Option(
                "Vyberte si prosím z možností"
            ));
            for (const [key,val] of Object.entries(PARO_ORGANIZATION_INFO.organization.districts)) {
                $districtsTarget.append(new Option(
                    val,
                    parseInt(key),
                ));
            }
        },
        showProjectLocation: function () {
            if (!projectDetail.mapy_loaded) {
                console.error('mapy not yet loaded');
                return;
            }
            let projectPosition = SMap.Coords.fromWGS84(16.60796, 49.19522);
            let $mapyTarget = $("#mapTarget").get(0);
            projectDetail.map = new SMap($mapyTarget, projectPosition, 15);
            projectDetail.map.addDefaultControls();

            projectDetail.map.addDefaultLayer(SMap.DEF_OPHOTO);
            projectDetail.map.addDefaultLayer(SMap.DEF_TURIST).enable();
            projectDetail.map.addDefaultLayer(SMap.DEF_BASE);

            let wmsLayer = new SMap.Layer.WMS('brno', 'https://gis.brno.cz/services/mapserver/local/nemovity_majetek/gservice',
                {
                    'layers': 'psmb',
                    'transparent': true,
                    'format': 'image/png'
                },
            );
            wmsLayer.getContainer()[SMap.LAYER_TILE].style.opacity = 0.7;
            projectDetail.map.addLayer(wmsLayer).enable();

            var layerSwitch = new SMap.Control.Layer({
                width: 65,
                items: 4,
                page: 4
            });
            layerSwitch.addDefaultLayer(SMap.DEF_BASE);
            layerSwitch.addDefaultLayer(SMap.DEF_OPHOTO);
            layerSwitch.addDefaultLayer(SMap.DEF_TURIST);
            projectDetail.map.addControl(layerSwitch, {left: "8px", top: "9px"});

            // projects markers layer
            projectDetail.projects_layer = new SMap.Layer.Marker();
            projectDetail.map.addLayer(projectDetail.projects_layer).enable();

            let mapSignals = projectDetail.map.getSignals();
            mapSignals.addListener(window, "map-click", projectDetail.setNewCoords);
            mapSignals.addListener(window, "marker-drag-stop", projectDetail.setNewCoords);

            projectDetail.marker = new SMap.Marker(projectPosition, 1, {
                title: ''
            });
            projectDetail.projects_layer.addMarker(projectDetail.marker);

            // dynamic resize
            let sync = new SMap.Control.Sync({});
            projectDetail.map.addControl(sync);
        },
        setNewCoords: function (event = undefined, coords = undefined, set_map_center = false) {
            coords = typeof coords == 'object' ? coords : SMap.Coords.fromEvent(event.data.event, projectDetail.map);
            window.coords = coords;
            projectDetail.marker.setCoords(coords);
            if (set_map_center === true) {
                projectDetail.map.setCenter(coords);
            }
            //.doCallback(coords);
        },
        run: function () {
            console.log('projectDetail run');

            // load mapy.cz async
            Loader.async = true;
            Loader.load(null, null, projectDetail.mapyLoadedCallback)

            // load organization
            $.fn.paro2.loadOrganization(projectDetail.projectLoadedCallback)
        }
    };

    $.fn.paro2.projectDetail = projectDetail;
    $.fn.paro2.projectDetail.run();

})(jQuery, window, document);