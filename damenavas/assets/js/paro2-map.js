;(function ($, window, document, undefined) {

    let mapGallery = {
        map: undefined,
        map_target_selector: "map-gallery",
        projects_layer: undefined,
        project_states_to_color: {},
        count_categories: {},
        count_states: {},
        count_districts: {},
        count_years: {},
        mapyLoadedCallback: function () {
            let center = SMap.Coords.fromWGS84(16.5079212, 49.2021611);
            let target = JAK.gel(mapGallery.map_target_selector);
            mapGallery.map = new SMap(target, center, 12);
            mapGallery.map.addDefaultControls();

            mapGallery.map.addDefaultLayer(SMap.DEF_OPHOTO);
            mapGallery.map.addDefaultLayer(SMap.DEF_TURIST).enable();
            mapGallery.map.addDefaultLayer(SMap.DEF_BASE);

            let wmsLayer = new SMap.Layer.WMS('brno', 'https://gis.brno.cz/services/mapserver/local/nemovity_majetek/gservice',
                {
                    'layers': 'psmb',
                    'transparent': true,
                    'format': 'image/png'
                },
            );
            wmsLayer.getContainer()[SMap.LAYER_TILE].style.opacity = 0.7;
            mapGallery.map.addLayer(wmsLayer).enable();

            var layerSwitch = new SMap.Control.Layer({
                width: 65,
                items: 4,
                page: 4
            });
            layerSwitch.addDefaultLayer(SMap.DEF_BASE);
            layerSwitch.addDefaultLayer(SMap.DEF_OPHOTO);
            layerSwitch.addDefaultLayer(SMap.DEF_TURIST);
            mapGallery.map.addControl(layerSwitch, {left: "8px", top: "9px"});

            // projects markers layer
            mapGallery.projects_layer = new SMap.Layer.Marker();
            mapGallery.map.addLayer(mapGallery.projects_layer).enable();

            // dynamic resize
            let sync = new SMap.Control.Sync({});
            mapGallery.map.addControl(sync);

            mapGallery.reloadMarkers();
        },
        appealLoadedCallback: function () {
            // make counts for further selects filling
            mapGallery.count_districts = {};
            mapGallery.count_categories = {};
            mapGallery.count_states = {};
            mapGallery.count_years[PARO_APPEAL_INFO.appeal.year] = 0;
            for (const val of Object.values(PARO_APPEAL_INFO.appeal.projects)) {
                mapGallery.count_states[val.current_status_id] = (mapGallery.count_states[val.current_status_id] || 0) + 1;
                mapGallery.count_years[PARO_APPEAL_INFO.appeal.year] = (mapGallery.count_years[PARO_APPEAL_INFO.appeal.year] || 0) + 1;
                if ('district' in val && typeof val.district === 'object') {
                    mapGallery.count_districts[val.district.id] = (mapGallery.count_districts[val.district.id] || 0) + 1;
                }
                for (const [key, value] of Object.entries(val.categories)) {
                    mapGallery.count_categories[key] = (mapGallery.count_categories[key] || 0) + 1;
                }
            }

            let $legendTarget = $("#map-legend");
            $legendTarget.empty();
            for (const [key, val] of Object.entries(PARO_ORGANIZATION_INFO.organization.project_states)) {
                $("<li id='project-state-" + key + "'>"
                    + "<img src='" + PARO_API_URL + '/api/v1/map_icon/' + ((mapGallery.project_states_to_color[key] || 'FFFFFF').replace('#', '')) + "'>"
                    + val.name
                    + "</li>")
                    .appendTo($legendTarget);
            }

            if ('districts' in PARO_ORGANIZATION_INFO.organization) {
                let $districtsTarget = $("select#district");
                mapGallery.filter_district_target = $districtsTarget;
                $districtsTarget.on('change', mapGallery.filterRefresh);
                $districtsTarget.empty();
                $districtsTarget.append(new Option('dle městské části', 'all'));
                for (const [key, val] of Object.entries(PARO_ORGANIZATION_INFO.organization.districts)) {
                    let _count = (mapGallery.count_districts[key] || 0);
                    let _option = new Option(val + " (" + _count + ") ", key)
                    if (_count < 1) {
                        _option.disabled = true;
                    }
                    $districtsTarget.append(_option);
                }
            }

            let $categoriesTarget = $("select#category");
            mapGallery.filter_category_target = $categoriesTarget;
            $categoriesTarget.on('change', mapGallery.filterRefresh);
            $categoriesTarget.empty();
            $categoriesTarget.append(new Option('dle oblasti projektu', 'all'));
            for (const [key, val] of Object.entries(PARO_ORGANIZATION_INFO.organization.project_categories)) {
                let _count = (mapGallery.count_categories[key] || 0);
                let _option = new Option(val.name + " (" + _count + ") ", key)
                if (_count < 1) {
                    _option.disabled = true;
                }
                $categoriesTarget.append(_option);
            }

            let $statesTarget = $("select#status");
            mapGallery.filter_state_target = $statesTarget;
            $statesTarget.on('change', mapGallery.filterRefresh);
            $statesTarget.empty();
            $statesTarget.append(new Option('dle stavu projektu', 'all'));
            for (const [key, val] of Object.entries(PARO_ORGANIZATION_INFO.organization.project_states)) {
                let _count = (mapGallery.count_states[key] || 0);
                let _option = new Option(val.name + " (" + _count + ") ", key)
                if (_count < 1) {
                    _option.disabled = true;
                }
                $statesTarget.append(_option);
                mapGallery.project_states_to_color[key] = val.color;
            }

            let $yearsTarget = $("select#year");
            mapGallery.filter_year_target = $yearsTarget;
            $yearsTarget.on('change', mapGallery.filterRefresh);
            $yearsTarget.empty();
            //$yearsTarget.append(new Option('Všechny ročníky'));
            for (const val of Object.values(PARO_ORGANIZATION_INFO.organization.appeals)) {
                $yearsTarget.append(new Option('Ročník ' + val.year, val.year));
            }
            $yearsTarget.val(PARO_API_CURRENT_YEAR);

            let $searchInputTarget = $("input.filter-search");
            mapGallery.filter_search_target = $searchInputTarget;
            $searchInputTarget.on('change keyup paste', mapGallery.filterRefresh);

            mapGallery.reloadMarkers();
            mapGallery.filterRefresh();
        },
        filter_year_target: undefined,
        filter_year: undefined,
        filter_state_target: undefined,
        filter_state: undefined,
        filter_category_target: undefined,
        filter_category: undefined,
        filter_district_target: undefined,
        filter_district: undefined,
        filter_search_target: undefined,
        filter_search: undefined,
        filterReset: function () {
            if (mapGallery.filter_search_target) {
                mapGallery.filter_search_target.val("");
            }
            if (mapGallery.filter_year_target) {
                mapGallery.filter_year_target.val(PARO_API_CURRENT_YEAR);
            }
            if (mapGallery.filter_state_target) {
                mapGallery.filter_state_target.val("all");
            }
            if (mapGallery.filter_category_target) {
                mapGallery.filter_category_target.val("all");
            }
            if (mapGallery.filter_district_target) {
                mapGallery.filter_district_target.val("all");
            }
            mapGallery.filterRefresh();
        },
        filterRefresh: function (event) {
            let filterChanged = false;
            if (mapGallery.filter_year_target) {
                let current_year = parseInt(mapGallery.filter_year_target.val());
                if (mapGallery.filter_year !== current_year) {
                    mapGallery.filter_year = current_year;
                    filterChanged = true;
                    PARO_API_CURRENT_YEAR = current_year;
                    $.fn.paro2.loadAppeal(mapGallery.appealLoadedCallback, true)
                }
            }
            if (mapGallery.filter_state_target) {
                let current_state = parseInt(mapGallery.filter_state_target.val());
                if (mapGallery.filter_state !== current_state) {
                    mapGallery.filter_state = current_state;
                    filterChanged = true;
                }
            }
            if (mapGallery.filter_category_target) {
                let current_category = parseInt(mapGallery.filter_category_target.val());
                if (mapGallery.filter_category !== current_category) {
                    mapGallery.filter_category = current_category;
                    filterChanged = true;
                }
            }
            if (mapGallery.filter_district_target) {
                let current_district = parseInt(mapGallery.filter_district_target.val());
                if (mapGallery.filter_district !== current_district) {
                    mapGallery.filter_district = current_district;
                    filterChanged = true;
                }
            }
            if (mapGallery.filter_search_target) {
                let current_search = mapGallery.filter_search_target.val().trim();
                if (mapGallery.filter_search !== current_search) {
                    mapGallery.filter_search = current_search;
                    filterChanged = true;
                }
            }
            if (filterChanged) {
                mapGallery.reloadMarkers();
            }
        },
        matchesFilter: function (project) {
            if (mapGallery.filter_year && parseInt(project.appeal_year) !== parseInt(mapGallery.filter_year)) {
                console.log('year: ' + parseInt(project.appeal_year) + " vs. " + parseInt(mapGallery.filter_year))
                return false;
            }
            if (mapGallery.filter_district && (!('district' in project) || parseInt(project.district.id) !== parseInt(mapGallery.filter_district))) {
                console.log('year: ' + ('district' in project ? parseInt(project.district.id) : 'N/A') + " vs. " + parseInt(mapGallery.filter_district))
                return false;
            }
            if (mapGallery.filter_state && parseInt(project.current_status_id) !== parseInt(mapGallery.filter_state)) {
                console.log('year: ' + parseInt(project.current_status_id) + " vs. " + parseInt(mapGallery.filter_state))
                return false;
            }
            if (mapGallery.filter_category) {
                let expectedCategory = parseInt(mapGallery.filter_category);
                let foundMatching = false;
                for (const id of Object.keys(project.categories)) {
                    if (parseInt(id) === expectedCategory) {
                        foundMatching = true;
                        break;
                    }
                }
                if (!foundMatching) {
                    console.log('category not found: ' + parseInt(mapGallery.filter_category));
                    return false;
                }
            }
            if (mapGallery.filter_search && mapGallery.filter_search.trim().length >= 2) {
                let term = mapGallery.filter_search.trim().toLowerCase();
                if (!JSON.stringify(project).toLowerCase().includes(term)) {
                    return false;
                }
            }
            return true;
        },
        reloadMarkers: function () {
            if (typeof mapGallery.map === "undefined"
                || typeof PARO_APPEAL_INFO === "undefined"
                || typeof mapGallery.projects_layer === "undefined") {
                return;
            }

            mapGallery.projects_layer.removeAll();

            for (const val of Object.values(PARO_APPEAL_INFO.appeal.projects)) {
                let position = SMap.Coords.fromWGS84(val.gps.longitude, val.gps.latitude);
                if (!mapGallery.matchesFilter(val)) {
                    console.log('project ' + val.id + ' does not match filter criteria');
                    continue;
                }
                if (!val.gps.longitude || !val.gps.latitude) {
                    console.log('marker for project ' + val.id + " not added, gps: " + position);
                    continue;
                }
                let status_color = mapGallery.project_states_to_color[val.current_status_id] || 'FFFFFF';
                let znacka = JAK.mel('div');
                var obrazek = JAK.mel('img', {src: PARO_API_URL + '/api/v1/map_icon/' + (status_color.replace('#', ''))});
                znacka.appendChild(obrazek);
                let marker = new SMap.Marker(position, val.id, {
                    title: val.name,
                    url: obrazek
                });
                let card = new SMap.Card();
                card.getHeader().innerHTML = "<strong>" + val.name + "</strong>";
                card.getBody().innerHTML = "<div>Stav projektu: " +
                    val.current_status_text +
                    "<br/>" +
                    "Navrhovatel: " +
                    val.proposer.name +
                    "<br/>" +
                    ('district' in val ? 'Městská část: ' + val.district.name + '<br/>' : '') +
                    'Rozpočet: ' +
                    Number(val.approved_price_total > 0 ? val.approved_price_total : val.proposed_price_total).toLocaleString('cs-CZ', {
                        style: 'currency',
                        currency: 'CZK'
                    }) +
                    (('image' in val && val.image != null) ? '<br/><img style="max-width: 50%;" src="' + PARO_API_URL + val.image + '">' : '') +
                    "<br/><br/>" +
                    "<a href='/projekt/?id=" + val.id + "' target='_blank'>Detail projektu</a>" +
                    "</div>";

                marker.decorate(SMap.Marker.Feature.Card, card);
                mapGallery.projects_layer.addMarker(marker);
                console.log("add marker on position " + position);
            }
        },
        run: function () {
            console.log('mapGallery run');
            // load mapy.cz async
            Loader.async = true;
            Loader.load(null, null, this.mapyLoadedCallback)

            // load organization
            $.fn.paro2.loadOrganization($.fn.paro2.loadAppeal, mapGallery.appealLoadedCallback)

            $(".show-filter").click(function () {
                $("#filter-block").show();
                $(this).hide();
                $(".reset-filter").show();
            });
            $(".reset-filter").click(function () {
                $("#filter-block").hide();
                $(this).hide();
                mapGallery.filterReset();
                $(".show-filter").show();
            });
        }
    };

    $.fn.paro2.mapGallery = mapGallery;
    $.fn.paro2.mapGallery.run();

})(jQuery, window, document);