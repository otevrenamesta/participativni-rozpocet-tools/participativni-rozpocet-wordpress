;(function ($, window, document, undefined) {

    let mapGallery = {
        project_states_to_color: {},
        project_categories_to_color: {},
        contrast_colors: {},
        count_categories: {},
        count_states: {},
        count_districts: {},
        count_years: {},
        inProgressData: {},
        loadProjectsInProgress: function () {
            $.get(
                PARO_API_URL + '/api/v1/organization/' + PARO_API_ORGANIZATION_ID + '/projects_in_progress.json'
            ).done(function (data) {
                mapGallery.inProgressData = data;
                mapGallery.appealLoadedCallback();
            })
        },
        appealLoadedCallback: function () {
            // make counts for further selects filling
            mapGallery.count_districts = {};
            mapGallery.count_categories = {};
            mapGallery.count_states = {};
            mapGallery.count_years = {};

            for (const val of Object.values(mapGallery.inProgressData)) {
                if ('categories' in val) {
                    for (const key of Object.keys(val.categories)) {
                        mapGallery.count_categories[key] = (mapGallery.count_categories[key] || 0) + 1;
                    }
                }
                if ('district' in val && typeof val.district === 'object') {
                    mapGallery.count_districts[val.district.id] = (mapGallery.count_districts[val.district.id] || 0) + 1;
                }
                mapGallery.count_years[val.appeal_year] = 1 + (mapGallery.count_years[val.appeal_year] || 0);
            }

            let $yearsTarget = $("select#years");
            mapGallery.filter_year_target = $yearsTarget;
            $yearsTarget.on('change', mapGallery.filterRefresh);
            $yearsTarget.empty();
            //$yearsTarget.append(new Option('Všechny ročníky', 'all'));
            for (const val of Object.values(PARO_ORGANIZATION_INFO.organization.appeals)) {
                $yearsTarget.append(new Option('Ročník ' + val.year, val.year));
            }

            mapGallery.filterRefresh();
        },
        filter_year_target: undefined,
        filter_year: undefined,
        filter_state_target: undefined,
        filter_state: undefined,
        filter_category_target: undefined,
        filter_category: undefined,
        filter_district_target: undefined,
        filter_district: undefined,
        filter_search_target: undefined,
        filter_search: undefined,
        filterRefresh: function (event) {
            let filterChanged = false;
            if (mapGallery.filter_year_target) {
                let current_year = parseInt(mapGallery.filter_year_target.val());
                if (mapGallery.filter_year !== current_year) {
                    mapGallery.filter_year = current_year;
                    filterChanged = true;
                }
            }
            if (mapGallery.filter_category_target) {
                let current_category = parseInt(mapGallery.filter_category_target.val());
                if (mapGallery.filter_category !== current_category) {
                    mapGallery.filter_category = current_category;
                    filterChanged = true;
                }
            }
            if (mapGallery.filter_district_target) {
                let current_district = parseInt(mapGallery.filter_district_target.val());
                if (mapGallery.filter_district !== current_district) {
                    mapGallery.filter_district = current_district;
                    filterChanged = true;
                }
            }
            if (filterChanged) {
                mapGallery.reloadProjects();
            }
        },
        reloadProjects: function () {
            let $projectsTarget = $("#projects-in-progress-container");
            let votesCount = 0;
            for (const project of Object.values(mapGallery.inProgressData)) {
                let $projectContainer = $("#project-" + project.id, $projectsTarget);
                if (!$projectContainer.length) {
                    $projectContainer = $("<div class=\"col-xs-12 vap-result-project\">          \n" +
                        "\n" +
                        "            <div class=\"row\">\n" +
                        "                <div class=\"col-md-7 col-md-offset-2 col-xs-11 col-xs-offset-1\">\n" +
                        "                    <div class=\"row\">\n" +
                        "                        <div class=\"col-xs-12 vap-project-name\">\n" +
                        "                                <span class=\"rank\"></span>\n" +
                        "                            <a class=\"paro2-project-title\" href=\"#\" target=\"_blank\"></a>\n" +
                        "                        </div>\n" +
                        "                    </div>\n" +
                        "                    <div class=\"row\">\n" +
                        "                        <div class=\"col-md-4 col-xs-12 vap-project-info paro2-project-budget-proposed\"></div>\n" +
                        "                        <div class=\"col-md-4 col-xs-12 vap-project-info paro2-project-district\"></div>\n" +
                        "                        <div class=\"col-md-4 col-xs-12 vap-project-info paro2-project-proposer\"></div>\n" +
                        "                    </div>\n" +
                        "                </div>\n" +
                        "                <div class=\"col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1\">\n" +
                        "                    <div class=\"row\">\n" +
                        "                        <div class=\"col-xs-12 vap-project-balance\">\n" +
                        "                            <span data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\">\n" +
                        "                                <span class=\"vap-project-balance-number\">4 337</span>\n" +
                        "                                <span class=\"vap-project-balance-label\"><i class=\"fa fa-plus-circle control-button plus-vote-icon green-plus fa\" aria-hidden=\"true\"></i></span>\n" +
                        "                            </span>\n" +
                        "                        </div>                                        \n" +
                        "                        <div class=\"col-xs-12 vap-project-info\">" +
                        "<span class=\"vap-project-votes\"><span class='paro2-project-upvotes'></span> <i class=\"fa fa-male control-button green-plus fa\" aria-hidden=\"true\"></i>" +
                        "</span>" +
                        "<span class=\"vap-project-votes\"><span class='paro2-project-downvotes'></span> <i class=\"fa fa-male control-button red-minus fa\" aria-hidden=\"true\"></i>" +
                        "</span>" +
                        "</div>                 \n" +
                        "                    </div>\n" +
                        "                </div>                   \n" +
                        "            </div>       \n" +
                        "    </div>");

                    $projectContainer.attr('id', 'project-' + project.id);
                    let title = $(".paro2-project-title", $projectContainer);
                    title.text(project.name);
                    title.attr('href', '/projekt/?id=' + project.id);
                    if (typeof (project.district || undefined) === 'object') {
                        $(".paro2-project-district", $projectContainer).text('Lokalita: ' + project.district.name);
                    } else {
                        $(".paro2-project-district", $projectContainer).parent().toggle(false);
                    }
                    $(".paro2-project-budget-proposed", $projectContainer).text($.fn.paro2.formatCurrency(project.proposed_price_total));
                    $(".paro2-project-proposer", $projectContainer).text('Navrhovatel: ' + project.proposer.name);
                    $(".vap-project-balance-number", $projectContainer).text(project.votes_overall);
                    $(".paro2-project-upvotes", $projectContainer).text(project.votes_overall_breakdown.positive);
                    $(".paro2-project-downvotes", $projectContainer).text(project.votes_overall_breakdown.negative);
                    $(".rank", $projectContainer).text(project.final_appeal_order);

                    votesCount += project.votes_overall_breakdown.positive + project.votes_overall_breakdown.negative;

                    $projectContainer.appendTo($projectsTarget);
                }
                let shouldShow = mapGallery.matchesFilter(project);
                $projectContainer.toggle(shouldShow);
            }
            $(".paro2-total-shown-votes").text(votesCount);
        },
        matchesFilter: function (project) {
            if (mapGallery.filter_year && parseInt(project.appeal_year) !== parseInt(mapGallery.filter_year)) {
                if (mapGallery.filter_year !== 'all') {
                    console.log('year: ' + parseInt(project.appeal_year) + " vs. " + parseInt(mapGallery.filter_year))
                    return false;
                }
            }
            if (mapGallery.filter_district && (!('district' in project) || parseInt(project.district.id) !== parseInt(mapGallery.filter_district))) {
                console.log('district: ' + ('district' in project ? parseInt(project.district.id) : 'N/A') + " vs. " + parseInt(mapGallery.filter_district))
                return false;
            }
            if (mapGallery.filter_category) {
                let expectedCategory = parseInt(mapGallery.filter_category);
                let foundMatching = false;
                for (const id of Object.keys(project.categories)) {
                    if (parseInt(id) === expectedCategory) {
                        foundMatching = true;
                        break;
                    }
                }
                if (!foundMatching) {
                    console.log('category not found: ' + parseInt(mapGallery.filter_category));
                    return false;
                }
            }
            if (mapGallery.filter_search && mapGallery.filter_search.trim().length >= 2) {
                let term = mapGallery.filter_search.trim().toLowerCase();
                if (!JSON.stringify(project).toLowerCase().includes(term)) {
                    return false;
                }
            }
            return true;
        },
        run: function () {
            console.log('projectsGallery run');

            // load organization
            $.fn.paro2.loadOrganization(mapGallery.loadProjectsInProgress)

            $(".show-filter").click(function () {
                $("#filter-block").show();
                $(this).hide();
                $(".reset-filter").show();
            });
            $(".reset-filter").click(function () {
                $("#filter-block").hide();
                $(this).hide();
                $(".show-filter").show();
            });
        }
    };

    $.fn.paro2.mapGallery = mapGallery;
    $.fn.paro2.mapGallery.run();

})(jQuery, window, document);