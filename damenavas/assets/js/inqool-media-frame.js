var file_frame;

jQuery(document).ready(function(){

    jQuery('form').submit(function(){
        var images = [];

        jQuery('.image-radio').each(function(){
            images.push(Number(jQuery(this).val()));
        });

        jQuery('#image_attachment_id').val(JSON.stringify(images));
    });

    jQuery('.image-block').on('click', '.image-remove', function(){
        var id = jQuery(this).data('id');
        jQuery('#image-box-' + id).remove()
    });

    jQuery('.attachment-block').on('click', '.file-remove', function(){
        var id = jQuery(this).data('id');
        jQuery('#document-box-' + id).remove();
        jQuery('.attachment-block').change();
    });

    jQuery('.file-block').on('click', '.file-remove', function(){
        var id = jQuery(this).data('id');
        jQuery('#document-box-' + id).remove()
    });


    jQuery('#upload-image .add-button').click( function( event ){
        var block = jQuery('#upload-image');
        createImageUpload(event, block);
    });

    jQuery('#upload-images .add-button').click( function( event ){
        var block = jQuery('#upload-images');
        createImagesUpload(event, block);
    });

    jQuery('#upload-images-cover .add-button').click( function( event ){
        var block = jQuery('#upload-images-cover');
        createImagesUploadWithCoverImage(event, block);
    });

    // Only for plugin with shortcode output
    jQuery('#upload-attachments .add-button').click( function( event ){
        var block = jQuery('#upload-attachments');
        createFilesUploadSmall(event, block);
    });

    jQuery('#upload-file .add-button').click( function( event ){
        var block = jQuery('#upload-file');
        createFileUpload(event, block);
    });

    jQuery('#upload-files .add-button').click( function( event ){
        var block = jQuery('#upload-files');
        createFilesUpload(event, block);
    });

});

function createImagesUploadWithCoverImage(event, block){
    event.preventDefault();

    file_frame = wp.media.frames.file_frame = wp.media({
        title: "Vyberte obrázky",
        button: {
        text: "Přidat",
        },
        multiple: true
    });

    file_frame.on( 'select', function(){
        var attachments = file_frame.state().get('selection').map( 
            function( attachment ) {
                attachment.toJSON();
                return attachment;
        });
        var i;
        for (i = 0; i < attachments.length; ++i) {
            var id = attachments[i]['attributes']['id']
            var url = attachments[i]['attributes']['url'];
            var html = 
                        '<div id="image-box-' + id + '" class="image-box">'
                            + '<input type="radio" class="image-radio" name="image_cover" value="' + id + '" id="img_' + id + '">'
                            + '<label for="img_' + id + '" class="image-label">'
                                + '<img id="image-preview" src="'+ url + '">'
                                + '<button class="image-remove" type="button" data-id="'+ id + '">odstranit</button>'
                            + '</label>'
                        + '</div>';
            jQuery(block).find(".image-block").append(html);
        }
    });
    file_frame.open();
}

function createImagesUpload(event, block){
    event.preventDefault();

    file_frame = wp.media.frames.file_frame = wp.media({
        title: "Vyberte obrázky",
        button: {
        text: "Přidat",
        },
        multiple: true
    });

    file_frame.on( 'select', function(){
        var attachments = file_frame.state().get('selection').map( 
            function( attachment ) {
                attachment.toJSON();
                return attachment;
        });
        var i;
        for (i = 0; i < attachments.length; ++i) {
            var id = attachments[i]['attributes']['id']
            var url = attachments[i]['attributes']['url'];
            var html = 
                        '<div id="image-box-' + id + '" class="image-box image-label">'
                            + '<img id="image-preview" src="' + url + '">'
                            + '<button class="image-remove" type="button" data-id="' + id + '">odstranit</button>'
                        + '</div>';
            jQuery(block).find(".image-block").append(html);
        }
    });
    file_frame.open();
}

function createImageUpload(event, block){
    event.preventDefault();

    file_frame = wp.media.frames.file_frame = wp.media({
        title: "Vyberte obrázek",
        button: {
        text: "Přidat",
        },
        multiple: false
    });

    file_frame.on( 'select', function(){
        var attachments = file_frame.state().get('selection').map( 
            function( attachment ) {
                attachment.toJSON();
                return attachment;
        });
        var i;
        for (i = 0; i < attachments.length; ++i) {
            var id = attachments[i]['attributes']['id']
            var url = attachments[i]['attributes']['url'];
            var html = 
                        '<div id="image-box-'+ id + '" class="image-box image-label">'
                           + '<img id="image-preview" src="'+ url + '">'
                           + '<button class="image-remove" type="button" data-id="' + id + '">odstranit</button>'
                           + '<input type="hidden" name="image_id" id="image_id" value="' + id + '">'                            
                        + '</div>';
            jQuery(block).find(".image-block").html(html);
        }
    });
    file_frame.open();
}

function createFileUpload(event, block){
    event.preventDefault();

    file_frame = wp.media.frames.file_frame = wp.media({
        title: "Vyberte soubor",
        button: {
        text: "Přidat",
        },
        multiple: false
    });

    file_frame.on( 'select', function(){
        var attachments = file_frame.state().get('selection').map( 
            function( attachment ) {
                attachment.toJSON();
                return attachment;
        });
        var i;
        for (i = 0; i < attachments.length; ++i) {
            var id = attachments[i]['attributes']['id'];
            var name = attachments[i]['attributes']['title'];
            var html = 
                    '<tr id="document-box-' + id + '" class="document-box">'
                        + '<td>' + name + '</td>'
                        + '<td><button type="button" class="file-remove button" data-id="' + id + '"><i class="fa fa-trash" aria-hidden="true"></i></button></td>'
                        + '<td><input type="hidden" name="file_id" id="file_id" value="' + id + '"></td>'
                    + '</tr>';
            jQuery(block).find(".file-block").html(html);
        }
    });
    file_frame.open();
}

function createFilesUploadSmall(event, block){
    event.preventDefault();

    file_frame = wp.media.frames.file_frame = wp.media({
        title: "Vyberte soboury",
        button: {
        text: "Přidat",
        },
        multiple: true
    });

    file_frame.on( 'select', function(){
        var attachments = file_frame.state().get('selection').map( 
            function( attachment ) {
                attachment.toJSON();
                return attachment;
        });
        var i;
        for (i = 0; i < attachments.length; ++i) {
            var id = attachments[i]['attributes']['id'];
            var name = attachments[i]['attributes']['title'].substring(0, 30);

            var html = 
            '<tr id="document-box-' + id + '" class="document-box">'
                + '<td class="attachment_title">' + name + '</td>'
                + '<td><button type="button" class="file-remove" data-id="' + id + '"><i class="fa fa-trash" aria-hidden="true"></i></button></td>'
                + '<td><input type="hidden" name="attachment_id[]" value="' + id + '"></td>'
            + '</tr>';

            jQuery(block).find(".attachment-block").append(html);
            jQuery('.attachment-block').change();
        }
    });
    file_frame.open();
}


function createFilesUpload(event, block){
    event.preventDefault();

    file_frame = wp.media.frames.file_frame = wp.media({
        title: "Vyberte soboury",
        button: {
        text: "Přidat",
        },
        multiple: true
    });

    file_frame.on( 'select', function(){
        var attachments = file_frame.state().get('selection').map( 
            function( attachment ) {
                attachment.toJSON();
                return attachment;
        });
        var i;
        for (i = 0; i < attachments.length; ++i) {
            var id = attachments[i]['attributes']['id']
            var name = attachments[i]['attributes']['title'];
            var html = 
            '<tr id="document-box-' + id + '" class="document-box">'
                + '<td>' + name + '</td>'
                + '<td><button type="button" class="file-remove button" data-id="' + id + '"><i class="fa fa-trash" aria-hidden="true"></i></button></td>'
                + '<td><input type="hidden" name="documents[]" value="' + id + '"></td>'
            + '</tr>';
            jQuery('.file-block').append(html);
        }
    });
    file_frame.open();
}





