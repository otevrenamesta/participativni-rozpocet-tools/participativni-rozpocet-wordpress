;(function ($, window, document, undefined) {

    let mapGallery = {
        project_states_to_color: {},
        project_categories_to_color: {},
        contrast_colors: {},
        count_categories: {},
        count_states: {},
        count_districts: {},
        count_years: {},
        appealLoadedCallback: function () {
            // make counts for further selects filling
            mapGallery.count_districts = {};
            mapGallery.count_categories = {};
            mapGallery.count_states = {};
            mapGallery.count_years[PARO_APPEAL_INFO.appeal.year] = 0;
            for (const val of Object.values(PARO_APPEAL_INFO.appeal.projects)) {
                mapGallery.count_states[val.current_status_id] = (mapGallery.count_states[val.current_status_id] || 0) + 1;
                mapGallery.count_years[PARO_APPEAL_INFO.appeal.year] = (mapGallery.count_years[PARO_APPEAL_INFO.appeal.year] || 0) + 1;
                if ('district' in val && typeof val.district === 'object') {
                    mapGallery.count_districts[val.district.id] = (mapGallery.count_districts[val.district.id] || 0) + 1;
                }
                if ('categories' in val) {
                    for (const [key, value] of Object.entries(val.categories)) {
                        mapGallery.count_categories[key] = (mapGallery.count_categories[key] || 0) + 1;
                    }
                }
            }

            if ('districts' in PARO_ORGANIZATION_INFO.organization) {
                let $districtsTarget = $("select#district");
                mapGallery.filter_district_target = $districtsTarget;
                $districtsTarget.on('change', mapGallery.filterRefresh);
                $districtsTarget.empty();
                $districtsTarget.append(new Option('dle městské části', 'all'));
                for (const [key, val] of Object.entries(PARO_ORGANIZATION_INFO.organization.districts)) {
                    let _count = (mapGallery.count_districts[key] || 0);
                    let _option = new Option(val + " (" + _count + ") ", key)
                    if (_count < 1) {
                        _option.disabled = true;
                    }
                    $districtsTarget.append(_option);
                }
            }

            let $categoriesTarget = $("select#category");
            mapGallery.filter_category_target = $categoriesTarget;
            $categoriesTarget.on('change', mapGallery.filterRefresh);
            $categoriesTarget.empty();
            $categoriesTarget.append(new Option('dle oblasti projektu', 'all'));
            for (const [key, val] of Object.entries(PARO_ORGANIZATION_INFO.organization.project_categories)) {
                let _count = (mapGallery.count_categories[key] || 0);
                let _option = new Option(val.name + " (" + _count + ") ", key)
                if (_count < 1) {
                    _option.disabled = true;
                }
                $categoriesTarget.append(_option);
                mapGallery.project_categories_to_color[parseInt(key)] = val.color;
                mapGallery.contrast_colors[val.color.toLowerCase()] = val.contrast_color;
            }

            let $statesTarget = $("select#status");
            mapGallery.filter_state_target = $statesTarget;
            $statesTarget.on('change', mapGallery.filterRefresh);
            $statesTarget.empty();
            $statesTarget.append(new Option('dle stavu projektu', 'all'));
            for (const [key, val] of Object.entries(PARO_ORGANIZATION_INFO.organization.project_states)) {
                let _count = (mapGallery.count_states[key] || 0);
                let _option = new Option(val.name + " (" + _count + ") ", key)
                if (_count < 1) {
                    _option.disabled = true;
                }
                $statesTarget.append(_option);
                mapGallery.project_states_to_color[parseInt(key)] = val.color;
                mapGallery.contrast_colors[val.color.toLowerCase()] = val.contrast_color;
            }

            let $yearsTarget = $("select#year");
            mapGallery.filter_year_target = $yearsTarget;
            $yearsTarget.on('change', mapGallery.filterRefresh);
            $yearsTarget.empty();
            //$yearsTarget.append(new Option('Všechny ročníky'));
            for (const val of Object.values(PARO_ORGANIZATION_INFO.organization.appeals)) {
                $yearsTarget.append(new Option('Ročník ' + val.year, val.year));
            }
            $yearsTarget.val(PARO_API_CURRENT_YEAR);

            let $searchInputTarget = $("input.filter-search");
            mapGallery.filter_search_target = $searchInputTarget;
            $searchInputTarget.on('change keyup paste', mapGallery.filterRefresh);

            mapGallery.filterRefresh();
        },
        filter_year_target: undefined,
        filter_year: undefined,
        filter_state_target: undefined,
        filter_state: undefined,
        filter_category_target: undefined,
        filter_category: undefined,
        filter_district_target: undefined,
        filter_district: undefined,
        filter_search_target: undefined,
        filter_search: undefined,
        filterReset: function () {
            if (mapGallery.filter_search_target) {
                mapGallery.filter_search_target.val("");
            }
            if (mapGallery.filter_year_target) {
                mapGallery.filter_year_target.val(PARO_API_CURRENT_YEAR);
            }
            if (mapGallery.filter_state_target) {
                mapGallery.filter_state_target.val("all");
            }
            if (mapGallery.filter_category_target) {
                mapGallery.filter_category_target.val("all");
            }
            if (mapGallery.filter_district_target) {
                mapGallery.filter_district_target.val("all");
            }
            mapGallery.filterRefresh();
        },
        filterRefresh: function (event) {
            let filterChanged = false;
            if (mapGallery.filter_year_target) {
                let current_year = parseInt(mapGallery.filter_year_target.val());
                if (mapGallery.filter_year !== current_year) {
                    mapGallery.filter_year = current_year;
                    filterChanged = true;
                    PARO_API_CURRENT_YEAR = current_year;
                    $.fn.paro2.loadAppeal(mapGallery.appealLoadedCallback, true)
                }
            }
            if (mapGallery.filter_state_target) {
                let current_state = parseInt(mapGallery.filter_state_target.val());
                if (mapGallery.filter_state !== current_state) {
                    mapGallery.filter_state = current_state;
                    filterChanged = true;
                }
            }
            if (mapGallery.filter_category_target) {
                let current_category = parseInt(mapGallery.filter_category_target.val());
                if (mapGallery.filter_category !== current_category) {
                    mapGallery.filter_category = current_category;
                    filterChanged = true;
                }
            }
            if (mapGallery.filter_district_target) {
                let current_district = parseInt(mapGallery.filter_district_target.val());
                if (mapGallery.filter_district !== current_district) {
                    mapGallery.filter_district = current_district;
                    filterChanged = true;
                }
            }
            if (mapGallery.filter_search_target) {
                let current_search = mapGallery.filter_search_target.val().trim();
                if (mapGallery.filter_search !== current_search) {
                    mapGallery.filter_search = current_search;
                    filterChanged = true;
                }
            }
            if (filterChanged) {
                mapGallery.reloadProjects();
            }
        },
        reloadProjects: function () {
            let $projectsTarget = $("#projects-gallery");
            $(".project-container", $projectsTarget).hide();
            let stats = {
                'votes': 0,
                'projects': 0,
                'milions': 0,
            };
            for (const project of Object.values(PARO_APPEAL_INFO.appeal.projects)) {
                let $projectContainer = $("#project-" + project.id, $projectsTarget);
                if (!$projectContainer.length) {
                    let bgColor = mapGallery.project_categories_to_color[Object.keys(project.categories || {})[0]] || '#ffffff';
                    let fgColor = mapGallery.contrast_colors[bgColor.toLowerCase()];
                    let statusFgColor = mapGallery.contrast_colors[mapGallery.project_states_to_color[project.current_status_id].toLowerCase()];
                    let shortFgCss = ';color: ' + fgColor + ";";
                    let longFgCss = ';color: ' + fgColor + "; -webkit-text-stroke: 0.5px " + bgColor + "; font-weight: bold;";
                    $projectContainer = $("<div class=\"project-container col-xs-6 col-sm-4\" style='height: 350px' id='project-" + project.id + "'>\n" +
                        "            <div class=\"project\" style=\"background: " + bgColor + "; color: " + fgColor + ";height: 350px;\">\n" +
                        "            <img class=\"hand-left\" style=\"top: 160px;\" src=\"https://damenavas.brno.cz/wp-content/themes/vega/assets/img/hand-left.png\">\n" +
                        "            <img class=\"hand-right\" style=\"top: 160px;\" src=\"https://damenavas.brno.cz/wp-content/themes/vega/assets/img/hand-right.png\">\n" +
                        "            <div class=\"col-xs-12 section-title text-center\" style='" + shortFgCss + "'>\n" +
                        project.name +
                        "            </div>\n" +
                        "            <div class=\"col-xs-12 section-image\">\n" +
                        "                    <div class=\"image\" style=\"background-image: url(" + (project.image ? PARO_API_URL + project.image : '') + ");\"></div>\n" +
                        "            </div>\n" +
                        "            <div class=\"col-xs-12 section-district text-center\">\n" +
                        (project.district ? project.district.name : '') +
                        "            </div>\n" +
                        "<div class=\"section-status\">\n" +
                        "                    <div class=\"status\">\n" +
                        "                        <!--?xml version=\"1.0\" encoding=\"UTF-8\"?-->\n" +
                        "\n" +
                        "<!-- Creator: CorelDRAW X7 -->\n" +
                        "<svg xmlns=\"http://www.w3.org/2000/svg\" xml:space=\"preserve\" width=\"130px\" height=\"50px\" version=\"1.1\" style=\"fill: " + mapGallery.project_states_to_color[project.current_status_id] + ";shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd\" viewBox=\"0 0 493 255\" preserveAspectRatio=\"none\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
                        " <defs>\n" +
                        " </defs>\n" +
                        " <g id=\"Vrstva_x0020_1\">\n" +
                        "  <metadata id=\"CorelCorpID_0Corel-Layer\"></metadata>\n" +
                        "  <path class=\"fil0\" d=\"M370 5c-23,0 -26,3 -47,4l-81 11c-64,14 -151,-13 -192,-7 -16,3 -10,12 -39,14 0,11 -8,52 -11,91l0 46c1,11 3,19 7,25l26 23c-16,23 -13,12 -6,43 53,0 63,-8 92,-13l126 -9c20,-1 18,-1 33,-1l97 -4c76,0 91,21 111,-57 9,-36 8,-47 4,-79 -4,-39 7,-56 -33,-59 -1,-22 -7,-31 -15,-33l-12 0c-10,2 -22,9 -28,9 -13,1 -11,-4 -32,-4z\"></path>\n" +
                        " </g>\n" +
                        "</svg>\n" +
                        "                        <span class=\"title\" style='color: " + statusFgColor + "'>\n" +
                        project.current_status_text +
                        "                        </span>\n" +
                        "                    </div>\n" +
                        "                </div>" +
                        "\n" +
                        "            <div class=\"project-hover\">\n" +
                        "                <div class=\"detail\">\n" +
                        "                    <a href=\"/projekt/?id=" + project.id + "\" target=\"_blank\">Prohlédnout projekt</a>\n" +
                        "                </div>\n" +
                        "                <div class=\"info\">\n" +
                        "                    <div class= \"font-white\"><strong>Rozpočet:</strong> \n" +
                        "                        " +
                        Number(project.approved_price_total > 0 ? project.approved_price_total : project.proposed_price_total).toLocaleString('cs-CZ', {
                            style: 'currency',
                            currency: 'CZK'
                        }) +
                        "</div>\n" +
                        "                    <div class=\"font-white\"><strong>Kategorie:</strong> " + Object.values(project.categories || []).join(', ') + "</div>\n" +
                        "                </div>\n" +
                        "            </div>\n" +
                        "        </div>\n" +
                        "\n" +
                        "    </div>");
                    $projectContainer.appendTo($projectsTarget);
                    textFit($(".section-title", $projectContainer), {
                        multiLine: true,
                        reProcess: true,
                        maxFontSize: 30
                    });
                }
                let shouldShow = mapGallery.matchesFilter(project);
                $projectContainer.toggle(shouldShow);
                if (shouldShow) {
                    stats.votes += ('votes_overall' in project ? project.votes_overall : 0);
                    stats.projects += 1;
                    stats.milions += ('proposed_price_total' in project ? project.proposed_price_total : 0);
                }
            }
            $(".g-stats-number.projectsLike").text(stats.votes);
            $(".g-stats-number.projectsBudget").text(Math.round(stats.milions / 1000000));
            $(".g-stats-number.projectsCount").text(stats.projects);
        },
        matchesFilter: function (project) {
            if (mapGallery.filter_year && parseInt(project.appeal_year) !== parseInt(mapGallery.filter_year)) {
                console.log('year: ' + parseInt(project.appeal_year) + " vs. " + parseInt(mapGallery.filter_year))
                return false;
            }
            if (mapGallery.filter_district && (!('district' in project) || parseInt(project.district.id) !== parseInt(mapGallery.filter_district))) {
                console.log('year: ' + ('district' in project ? parseInt(project.district.id) : 'N/A') + " vs. " + parseInt(mapGallery.filter_district))
                return false;
            }
            if (mapGallery.filter_state && parseInt(project.current_status_id) !== parseInt(mapGallery.filter_state)) {
                console.log('year: ' + parseInt(project.current_status_id) + " vs. " + parseInt(mapGallery.filter_state))
                return false;
            }
            if (mapGallery.filter_category) {
                let expectedCategory = parseInt(mapGallery.filter_category);
                let foundMatching = false;
                for (const id of Object.keys(project.categories)) {
                    if (parseInt(id) === expectedCategory) {
                        foundMatching = true;
                        break;
                    }
                }
                if (!foundMatching) {
                    console.log('category not found: ' + parseInt(mapGallery.filter_category));
                    return false;
                }
            }
            if (mapGallery.filter_search && mapGallery.filter_search.trim().length >= 2) {
                let term = mapGallery.filter_search.trim().toLowerCase();
                if (!JSON.stringify(project).toLowerCase().includes(term)) {
                    return false;
                }
            }
            return true;
        },
        run: function () {
            console.log('projectsGallery run');

            // load organization
            $.fn.paro2.loadOrganization($.fn.paro2.loadAppeal, mapGallery.appealLoadedCallback)

            $(".show-filter").click(function () {
                $("#filter-block").show();
                $(this).hide();
                $(".reset-filter").show();
            });
            $(".reset-filter").click(function () {
                $("#filter-block").hide();
                $(this).hide();
                $(".show-filter").show();
            });
        }
    };

    $.fn.paro2.mapGallery = mapGallery;
    $.fn.paro2.mapGallery.run();

})(jQuery, window, document);