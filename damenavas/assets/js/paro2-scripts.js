;function unixTimestampToDate(timestamp) {
    timestamp = parseInt(timestamp) * 1000;
    return (new Date(timestamp)).toLocaleDateString('cs-CZ');
}

;(function ($, window, document, undefined) {

    $.fn.paro2 = {};

    $.fn.paro2.formatCurrency = function(amount) {
        return Number(amount).toLocaleString('cs-CZ', {
            style: 'currency',
            currency: 'CZK'
        })
    }

    $.fn.paro2.getUrlParameter = function (sParam) {
        let sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    };

    $.fn.paro2.loadOrganization = function(callback = undefined, sub_callback = undefined, force = false) {
        if (PARO_ORGANIZATION_INFO && !force) {
            console.log('paro2_loadOrganization: organization is already loaded, not running XHR');
            if (typeof callback === 'function') {
                callback(sub_callback);
            }
            return;
        }
        $.get(PARO_API_URL + '/api/v1/organization/' + PARO_API_ORGANIZATION_ID + '/info.json')
            .done(function (data) {
                PARO_ORGANIZATION_INFO = data;
                if (typeof callback === 'function') {
                    callback(sub_callback);
                }
            });
    }

    $.fn.paro2.loadAppeal = function(callback = undefined, force = false) {
        if (PARO_APPEAL_INFO && !force) {
            console.log('paro2_loadAppeal: appeal already loaded, not running XHR');
            if (typeof callback === 'function') {
                callback();
            }
        }

        if (!('organization' in PARO_ORGANIZATION_INFO) || !('appeals' in PARO_ORGANIZATION_INFO.organization)) {
            console.error('paro2_loadAppeal: PARO_ORGANIZATION_INFO is missing data required for loadAppeal');
            return;
        }

        let appeal_api_url = undefined;
        for (const val of Object.values(PARO_ORGANIZATION_INFO.organization.appeals)) {
            if (val.year === PARO_API_CURRENT_YEAR) {
                appeal_api_url = val.url;
            }
        }
        if (typeof appeal_api_url === "undefined") {
            console.error('paro2_loadAppeal: Appeal for year ' + PARO_API_CURRENT_YEAR + " not found");
            return;
        }
        $.get(PARO_API_URL + appeal_api_url)
            .done(function (data) {
                PARO_APPEAL_INFO = data;
                if (typeof callback === 'function') {
                    callback();
                }
            });
    }

    $.fn.paro2.loadProject = function(callback = undefined, force = false) {
        if (PARO_PROJECT_INFO && !force) {
            console.log('paro2_loadProject: project already loaded, not running XHR');
            if (typeof callback === 'function') {
                callback();
            }
        }

        if (!('organization' in PARO_ORGANIZATION_INFO) || !('appeals' in PARO_ORGANIZATION_INFO.organization)) {
            console.error('paro2_loadProject: PARO_ORGANIZATION_INFO is missing data required for loadProject');
            return;
        }

        $.get(PARO_API_URL + '/api/v1/organization/' + PARO_API_ORGANIZATION_ID + "/project/" + $.fn.paro2.getUrlParameter('id') + "/info.json")
            .done(function (data) {
                if ('project' in data) {
                    data = data.project;
                }
                PARO_PROJECT_INFO = data;
                if (typeof callback === 'function') {
                    callback();
                }
            });
    }

    $.fn.paro2.showHarmonogram = function() {
        if (!PARO_APPEAL_INFO || !('appeal' in PARO_APPEAL_INFO) || !('phases' in PARO_APPEAL_INFO.appeal)) {
            console.error('paro2_showHarmonogram: missing appeal info');
            return;
        }

        const selector = '#harmonogram-target';
        let $target = $(selector);
        if (!$target) {
            console.error('paro2_showHarmonogram: missing element for selector ' + selector);
            return;
        }

        $target.empty();

        for (const key of Object.keys(PARO_APPEAL_INFO.appeal.phases).sort()) {
            let val = PARO_APPEAL_INFO.appeal.phases[key];
            $('<div class="fifth-left-outer"><div class="fifth-left-title">' +
                val.name + (val.currently_active === true ? ' <span class="text-success">(probíhá nyní)</span>' : '') +
                '</div><div class="fifth-left-date">' +
                unixTimestampToDate(val.date_start) +
                " - " +
                unixTimestampToDate(val.date_end) +
                '</div></div>'
            ).appendTo($target);
        }
    }

})(jQuery, window, document);