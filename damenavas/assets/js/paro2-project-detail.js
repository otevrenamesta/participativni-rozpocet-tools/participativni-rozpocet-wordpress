;(function ($, window, document, undefined) {

    let projectDetail = {
        map: undefined,
        mapy_loaded: false,
        map_target_selector: 'project',
        projects_layer: undefined,
        mapyLoadedCallback: function () {
            projectDetail.mapy_loaded = true;
            projectDetail.showProjectLocation();
        },
        projectLoadedCallback: function () {
            projectDetail.showProjectLocation();
            let project_category_id = Object.keys(PARO_PROJECT_INFO.categories)[0] || 1;
            let category = PARO_ORGANIZATION_INFO.organization.project_categories[project_category_id];
            if (category) {
                $(".project-background-color").css('background-color', category.color);
                $(".project-font-color").css('color', category.contrast_color);
                $(".project-font-color-invert").css('color', category.color);
                $(".project-background-color-invert").css('background-color', category.contrast_color);
            }

            $(".project-description").html(PARO_PROJECT_INFO.description);
            $publicInterestTarget = $("#project-public-interest-content");
            $publicInterestTarget.html(PARO_PROJECT_INFO.public_interest_description);
            if (!PARO_PROJECT_INFO.public_interest_description || PARO_PROJECT_INFO.public_interest_description.length < 2) {
                $publicInterestTarget.parent().parent().parent().parent().hide();
            }
            let $projectTitle = $(".project-title");
            $projectTitle.text(PARO_PROJECT_INFO.name)
            $(window).resize(function () {
                let $projectTitle = $(".project-title");
                $projectTitle.height(Math.min(250, $(".project-header-image").height() - 50));
                try {
                    textFit($projectTitle, {
                        multiLine: true,
                        reProcess: true,
                        alignVert: true,
                        alignHoriz: false,
                        detectMultiline: false,
                        alignVertWithFlexbox: true
                    });
                } catch (e) {
                    console.log(e);
                }
                //$projectTitle.css('line-height', (parseInt($(".textFitted", $projectTitle).css('font-size')) * 0.9) + 'px');
            });
            setTimeout(() => $(window).trigger('resize'), 100);

            if ('district' in PARO_PROJECT_INFO) {
                $(".project-district-name").text(PARO_PROJECT_INFO.district.name);
            } else {
                $(".project-district-info-container").hide();
            }
            $(".project-appeal-year").text(PARO_PROJECT_INFO.appeal_year);
            $(".project-categories").text(Object.values(PARO_PROJECT_INFO.categories).join(', '));
            $(".project-proposer-name").text(PARO_PROJECT_INFO.proposer.name);
            $(".project-like-count").text(PARO_PROJECT_INFO.votes_overall);
            $(".project-annotation").text(PARO_PROJECT_INFO.annotation);
            $(".project-description").html(PARO_PROJECT_INFO.description);

            if (PARO_PROJECT_INFO.has_no_location === true) {
                $(".paro2-show-map-button").toggle(false);
                $("#scroll-map").toggle(false);
            }

            if (PARO_PROJECT_INFO.video_url !== null) {
                let videoEl = $("<div>\n" +
                    "<h3 class=\"font-extra-bold text-center mt-0\">Hlasovací video <i class=\"fa fa-plus project-show-detail\" aria-hidden=\"true\" data-id=\"project-block-realization-gallery\"></i></h3>\n" +
                    "<div id=\"project-block-realization-gallery\" class=\"embed-responsive embed-responsive-16by9 align-middle\" style=\"display: none;\">\n" +
                    "<iframe class=\"embed-responsive-item\" \n" +
                    "allowfullscreen=\"allowfullscreen\"\n" +
                    "mozallowfullscreen=\"mozallowfullscreen\" \n" +
                    "msallowfullscreen=\"msallowfullscreen\" \n" +
                    "oallowfullscreen=\"oallowfullscreen\" \n" +
                    "webkitallowfullscreen=\"webkitallowfullscreen\"\n" +
                    "src=\"" + PARO_PROJECT_INFO.video_url + "\">\n" +
                    "</iframe>\n" +
                    "</div>\n" +
                    "</div>");
                $(".project-show-detail", videoEl).click(function () {
                    $("#" + $(this).data('id')).slideToggle();
                });
                $(".project-description").append(videoEl);
            }

            let $projectPriceProposed = $(".project-price-proposed");
            let $projectPriceApproved = $(".project-price-approved");

            $projectPriceProposed.text(
                Number(PARO_PROJECT_INFO.proposed_price_total).toLocaleString('cs-CZ', {
                    style: 'currency',
                    currency: 'CZK'
                }) + " (navržený)"
            );
            $projectPriceApproved.text(
                Number(PARO_PROJECT_INFO.approved_price_total).toLocaleString('cs-CZ', {
                    style: 'currency',
                    currency: 'CZK'
                }) + " (schválený)"
            );
            if (PARO_PROJECT_INFO.approved_price_total < 1) {
                $projectPriceApproved.parent().hide();
            }

            let $diaryTarget = $("#project-block-diary table tbody");
            for (const val of Object.values(PARO_PROJECT_INFO.project_history)) {
                $("<tr>" +
                    "<td>" + unixTimestampToDate(val.timestamp) + "</td>" +
                    "<td>" + val.title + "</td>" +
                    "</tr>").appendTo($diaryTarget);
            }

            let $proposedTableTarget = $("#project-budget-proposed-items");
            for (const val of Object.values(PARO_PROJECT_INFO.budget.proposed)) {
                $proposedTableTarget.append(
                    $("<tr>" +
                        "<td>" + val.description + "</td>" +
                        "<td class=\"price text-center\">" + val.item_count + "</td>" +
                        "<td class=\"price text-right\">" + $.fn.paro2.formatCurrency(val.item_price) + "</td>" +
                        "<td class=\"price text-right\">" + $.fn.paro2.formatCurrency(val.total_price) + "</td>" +
                        "</tr>")
                );
            }
            $("#project-budget-approved-total").text($.fn.paro2.formatCurrency(PARO_PROJECT_INFO.proposed_price_total));


            let $feasibilityTarget = $("#project-feasibility-items");
            for (const val of Object.values(PARO_PROJECT_INFO.feasibility_statements)) {
                $feasibilityTarget.append(
                    $("<tr>" +
                        "<td>" + val.organization_part + "</td>" +
                        "<td class=\"text-left\">" + val.text + "</td>" +
                        "<td class=\"price text-right\">" + val.title + "</td>" +
                        "</tr>")
                );
            }

            let $approvedTableTarget = $("#project-budget-approved-items");
            for (const val of Object.values(PARO_PROJECT_INFO.budget.approved)) {
                $approvedTableTarget.append(
                    $("<tr>" +
                        "<td>" + val.description + "</td>" +
                        "<td class=\"price text-center\">" + val.item_count + "</td>" +
                        "<td class=\"price text-right\">" + $.fn.paro2.formatCurrency(val.item_price) + "</td>" +
                        "<td class=\"price text-right\">" + $.fn.paro2.formatCurrency(val.total_price) + "</td>" +
                        "</tr>")
                );
            }
            $("#project-budget-proposed-total").text($.fn.paro2.formatCurrency(PARO_PROJECT_INFO.proposed_price_total));

            if (!('voting' in PARO_PROJECT_INFO)) {
                $("#project-detail-like").hide();
                $("#project-detail-dislike").hide();
            } else {
                if ('positive_url' in PARO_PROJECT_INFO.voting) {
                    $("#project-detail-like").click(function () {
                        if ('voting' in PARO_PROJECT_INFO) {
                            window.open(PARO_API_URL + PARO_PROJECT_INFO.voting.positive_url + "?ref=" + window.location.href, '_self');
                        } else {
                            console.log('cannot vote since voting is not possible in curent project status')
                        }
                    });
                } else {
                    $("#project-detail-like").toggle(false);
                }
                if ('negative_url' in PARO_PROJECT_INFO.voting) {
                    $("#project-detail-dislike").click(function () {
                        if ('voting' in PARO_PROJECT_INFO) {
                            window.open(PARO_API_URL + PARO_PROJECT_INFO.voting.negative_url + "?ref=" + window.location.href, '_self');
                        } else {
                            console.log('cannot vote since voting is not possible in curent project status')
                        }
                    });
                } else {
                    $("#project-detail-dislike").toggle(false);
                }
            }
        },
        showProjectLocation: function () {
            if (!projectDetail.mapy_loaded || !PARO_PROJECT_INFO) {
                console.log('mapy.cz or project info not yet loaded');
                return;
            }
            let projectPosition = SMap.Coords.fromWGS84(PARO_PROJECT_INFO.gps.longitude, PARO_PROJECT_INFO.gps.latitude);
            let $mapyTarget = $("#project-detail-map").get(0);
            projectDetail.map = new SMap($mapyTarget, projectPosition, 15);
            projectDetail.map.addDefaultControls();

            projectDetail.map.addDefaultLayer(SMap.DEF_OPHOTO);
            projectDetail.map.addDefaultLayer(SMap.DEF_TURIST).enable();
            projectDetail.map.addDefaultLayer(SMap.DEF_BASE);

            let wmsLayer = new SMap.Layer.WMS('brno', 'https://gis.brno.cz/services/mapserver/local/nemovity_majetek/gservice',
                {
                    'layers': 'psmb',
                    'transparent': true,
                    'format': 'image/png'
                },
            );
            wmsLayer.getContainer()[SMap.LAYER_TILE].style.opacity = 0.7;
            projectDetail.map.addLayer(wmsLayer).enable();

            var layerSwitch = new SMap.Control.Layer({
                width: 65,
                items: 4,
                page: 4
            });
            layerSwitch.addDefaultLayer(SMap.DEF_BASE);
            layerSwitch.addDefaultLayer(SMap.DEF_OPHOTO);
            layerSwitch.addDefaultLayer(SMap.DEF_TURIST);
            projectDetail.map.addControl(layerSwitch, {left: "8px", top: "9px"});

            // projects markers layer
            projectDetail.projects_layer = new SMap.Layer.Marker();
            projectDetail.map.addLayer(projectDetail.projects_layer).enable();

            let marker = new SMap.Marker(projectPosition, PARO_PROJECT_INFO.id, {
                title: PARO_PROJECT_INFO.name
            });
            projectDetail.projects_layer.addMarker(marker);

            // dynamic resize
            let sync = new SMap.Control.Sync({});
            projectDetail.map.addControl(sync);
        },
        run: function () {
            console.log('projectDetail run');

            // load mapy.cz async
            Loader.async = true;
            Loader.load(null, null, projectDetail.mapyLoadedCallback)

            // load organization
            $.fn.paro2.loadOrganization($.fn.paro2.loadProject, projectDetail.projectLoadedCallback)
        }
    };

    $.fn.paro2.projectDetail = projectDetail;
    $.fn.paro2.projectDetail.run();

})(jQuery, window, document);