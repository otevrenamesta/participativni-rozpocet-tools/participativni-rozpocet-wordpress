<?php

require_once(ABSPATH . WPINC . '/class-wp-customize-setting.php');
require_once(ABSPATH . WPINC . '/class-wp-customize-section.php');
require_once(ABSPATH . WPINC . '/class-wp-customize-control.php');

class RichText_Editor_Custom_Control extends WP_Customize_Control
{
    function __construct($manager, $id, $options)
    {
        parent::__construct($manager, $id, $options);

        global $numRichText_Editor_Custom_Control_init;
        $numRichText_Editor_Custom_Control_init = empty($numRichText_Editor_Custom_Control_init)
            ? 1
            : $numRichText_Editor_Custom_Control_init + 1;
    }

    public function enqueue()
    {
        wp_enqueue_editor();
        parent::enqueue();
    }

    public function render_content()
    {
        global $numRichText_Editor_Custom_Control_init, $numRichText_Editor_Custom_Control_render;
        $numRichText_Editor_Custom_Control_render = empty($numRichText_Editor_Custom_Control_render)
            ? 1
            : $numRichText_Editor_Custom_Control_render + 1;
        ?>
        <label>
            <span class="customize-text_editor"><?php echo esc_html($this->label); ?></span>
            <input id="<?php echo $this->id ?>-link" class="wp-editor-area" type="hidden" <?php $this->link(); ?> value="<?php echo esc_textarea($this->value()); ?>">

            <?php $settings = [
                'textarea_name' => $this->id,
                'tinymce' => [
                    'setup' => "function (editor) {
                  var cb = function () {
                    var linkInput = document.getElementById('$this->id-link')
                    linkInput.value = editor.getContent()
                    linkInput.dispatchEvent(new Event('change'))
                  }
                  editor.on('Change', cb)
                  editor.on('Undo', cb)
                  editor.on('Redo', cb)
                  editor.on('KeyUp', cb) // Remove this if it seems like an overkill
                }"
                ]
            ];
            wp_editor($this->value(), $this->id, $settings); ?>
        </label>
        <?php
        if ($numRichText_Editor_Custom_Control_render === $numRichText_Editor_Custom_Control_init)
            do_action('admin_print_footer_scripts');
    }

}