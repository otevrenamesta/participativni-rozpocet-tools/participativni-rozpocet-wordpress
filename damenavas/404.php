<?php get_header(); ?>

<?php // get_template_part('parts/banner'); ?>

<!-- ========== Page Content ========== -->
<div class="section error-content bg-white">
    <div class="container">
        <h2>404</h2>
        <h3><?= __('Zdá se, že stránku, kterou hledáte, nemůžeme najít.') ?></h3>
        <h4>
            Zkuste se vrátit na <a href="/">úvodní stránku</a> nebo nám napište co se stalo na e-mail damenavas@brno.cz.
        </h4>
        <h5>Dáme na vás a opravíme to!</h5>
        <div style="height:200px"></div>
    </div>
</div>
<!-- ========== /Page Content ========== -->

<?php get_footer(); ?>
