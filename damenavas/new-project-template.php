<?php /* Template Name: new-project-template */ ?>
<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php //get_template_part('parts/banner'); ?>

<?php 
    global $post;
    $size = "full";
    $imageUrl = get_the_post_thumbnail_url($post, $size);   
?>
            <div class="container-fluid page-image-container" style="margin-bottom: 0">
            	<div class="row page-image-row">
                    <div class="col-sm-8 col-sm-offset-2 title">
                        <h1 style="min-height: 120px;"><?php echo get_the_title(); ?></h1>
	    	    </div>
                    <?php if($imageUrl != ""){ ?>
                        <div class="col-sm-5 image" style="/*background: url(<?php  echo($imageUrl); ?>);*/">
                            <img src="<?php  echo($imageUrl); ?>" style="/*visibility: hidden;*/" />
                        </div>
                    <?php } ?>
        	</div>
            </div>

<!-- ========== Page Content ========== -->
<div id="page-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
    
    <div class="page-content">
    <?php the_content(); ?>
    </div>
    
</div>
<!-- ========== /Page Content ========== -->

<?php endwhile; ?>

<?php get_footer(); ?>