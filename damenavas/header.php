<?php
/**
 * The Header for the theme.
 *
 * Displays all of the <head> section and logo and navigation
 *
 * @package vega
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="theme-color" content="#da2128">
    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>

    <script type="text/javascript">
        const PARO_API_URL = '<?= get_theme_mod('api_url') ?>';
        const PARO_API_ORGANIZATION_ID = <?= get_theme_mod('api_organization_id') ?>;
        let PARO_API_CURRENT_YEAR = <?= get_theme_mod('api_current_year') ?>;
        let PARO_ORGANIZATION_INFO = undefined;
        let PARO_APPEAL_INFO = undefined;
        let PARO_PROJECT_INFO = undefined;
    </script>

    <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext"
          rel="stylesheet">

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<?php get_template_part('parts/header', 'top'); ?>
<div class="red-fixed-row"></div>
<div class="red-fixed-row-animate" style="display: none;"></div>

<div class="clearfix" role="navigation">
    <div class="container-fluid p-header">
        <div class="row menu-row">
            <div class="col-sm-2">
                <a href="<?php echo get_site_url(); ?>" class="p-header-logo">
                    <?php if (has_custom_logo()) : ?>
                        <div class="site-logo my-paro-logo"><?php the_custom_logo(); ?></div>
                    <?php else: ?>
                        <div class="my-paro-logo"></div>
                    <?php endif; ?>
                </a>
            </div>
            <div class="<?php if (!is_home()) {
                echo "col-sm-6";
            } else {
                echo "col-sm-9";
            } ?> navbar-container">
                <?php if (has_nav_menu('header')) : ?>
                    <?php wp_nav_menu(array(
                            'theme_location' => 'header',
                            'depth' => 3,
                            'container' => 'div',
                            'container_class' => 'navbar-custom navbar-collapse collapse my-navbar p-header-menu',
                            'menu_class' => 'nav navbar-nav navbar-left menu-header',
                            'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                            'walker' => new wp_bootstrap_navwalker()
                        )
                    );
                    ?>
                <?php endif; ?>
            </div>
            <?php
            global $post;
            if (isset($post)) {
                $post_slug = $post->post_name;
            }
            ?>
            <?php
            $template = get_post_meta(get_the_ID(), '_wp_page_template', true);
            ?>

            <?php if (!is_home() && isset($post_slug)) { ?>
                <div class="col-sm-3 new-project-container">

                    <div class="new-project-block">
                        <?php if (basename(get_permalink()) != "hlasovani") { ?>
                            <div class="outer-button">
                                <a href="<?php echo get_option('header_link'); ?>"
                                   class="new-project-button p-button"><?php echo get_option('header_text'); ?></a>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            <?php } ?>
            <div class="search-button-block hidden-sm">
                <div class="outer-search">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ========== /Navbar ========== -->
